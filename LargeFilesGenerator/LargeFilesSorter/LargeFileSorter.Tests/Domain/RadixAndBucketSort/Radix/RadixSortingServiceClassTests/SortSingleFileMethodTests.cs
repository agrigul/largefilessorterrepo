﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LargeFileSorter.Domain.MergeSort;
using LargeFileSorter.Domain.RadixAndBucketSort.Radix;
using LargeFileSorter.Domain.RadixAndBucketSort.Radix.Splitting;
using Microsoft.VisualBasic;
using NUnit.Framework;

namespace LargeFileSorter.Tests.Domain.RadixAndBucketSort.Radix.RadixSortingServiceClassTests
{
    [TestFixture]
    public class SortSingleFileMethodTests : TestsBase
    {

        readonly RadixSortingService service = new RadixSortingService();

        [Test]
        [Description("Should sort file with small array")]
        public async Task EqualLengthFiles_Sorted()
        {
            IComparer<string> comparer = new TextThenNumberComparer();
            var testData = TestData.DescSortedStrings;

            var expectedResult = testData.OrderBy(x => x, comparer).ToList();

            var unsortedArray = testData;
            string pathToSourceFile = Path.Combine(WorkingDirectory, "SmallTestDataFile.txt");
            await File.WriteAllLinesAsync(pathToSourceFile, unsortedArray);

            await service.SortSingleFileWithMergeSort(pathToSourceFile);

            var result = await File.ReadAllLinesAsync(pathToSourceFile);
            for (var i = 0; i < expectedResult.Count; i++)
            {
                Assert.AreEqual(expectedResult[i], result[i]);
            }
        }


        [Test]
        [Description("Should sort regular file with small array")]
        public async Task RegularFiles_Sorted()
        {
            IComparer<string> comparer = new TextThenNumberComparer();
            var testData = TestData.RandomStrings;

            var expectedResult = testData.OrderBy(x => x, comparer).ToList();

            var unsortedArray = testData;
            string pathToSourceFile = Path.Combine(WorkingDirectory, "SmallTestDataFile.txt");
            await File.WriteAllLinesAsync(pathToSourceFile, unsortedArray);

            await service.SortSingleFileWithMergeSort(pathToSourceFile);

            var result = await File.ReadAllLinesAsync(pathToSourceFile);
            for (var i = 0; i < expectedResult.Count; i++)
            {
                Assert.AreEqual(expectedResult[i], result[i]);
            }
        }



        [TestCase(4)]
        [TestCase(8)]
        [TestCase(16)]
        [TestCase(32)]
        [TestCase(64)]
        [Explicit]
        [Description("Should sort. Different File size performance")]
        public async Task PerformanceTestOfSingleChunk(long fileSizeMb)
        {

            long fileSize = fileSizeMb * 1024 * 1024;

            var chunks = await GenerateFileForTesting(HundredMbFile, fileSize);
            var start = DateAndTime.Now;
            foreach (var chunk in chunks)
            {
                await service.SortSingleFileWithMergeSort(chunk);
            }


            Console.WriteLine($"Sorted {chunks.Count} files, size {fileSizeMb} MB in {(DateAndTime.Now - start).TotalSeconds}");
        }


        private async Task<IList<string>> GenerateFileForTesting(string fileName, long maxFileSize)
        {
            var testDataFilePath = CopyLargeArrays(fileName);
            var splitService = new SplitByAlphabet();
            var result = await splitService.SplitFiles(testDataFilePath, maxFileSize);

            return result;

        }
    }
}
