﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LargeFileSorter.Domain.MergeSort;
using LargeFileSorter.Domain.RadixAndBucketSort.Radix;
using LargeFileSorter.Domain.RadixAndBucketSort.Radix.Splitting;
using NUnit.Framework;

namespace LargeFileSorter.Tests.Domain.RadixAndBucketSort.Radix.RadixSortingServiceClassTests
{
    [TestFixture]
    public class SortAsynMethodTests : TestsBase
    {

        readonly RadixSortingService service = new RadixSortingService();

        [OneTimeSetUp]
        public virtual void OneTime()
        {
            base.OneTime();
            //WarmingUp().Wait();
        }

        private async Task WarmingUp()
        {
            var chunks = await GenerateFileForTesting(HundredMbFile, 1024 * 1024 * 4);
            await service.SortSingleFileWithRadixSort(chunks.First());
            await service.SortSingleFileWithMergeSort(chunks.Last());
        }


        private async Task<IList<string>> GenerateFileForTesting(string fileName, long maxFileSize)
        {
            var testDataFilePath = CopyLargeArrays(fileName);
            var splitService = new SplitByAlphabet();
            var result = await splitService.SplitFiles(testDataFilePath, maxFileSize);

            return result;

        }

        [Test]
        [Description("Should  split, sort and merge file with small array")]
        public async Task RegularFile_Sorted()
        {
            IComparer<string> comparer = new TextThenNumberComparer();
            var testData = TestData.RandomStrings;

            var expectedResult = testData.OrderBy(x => x, comparer).ToList();

            var unsortedArray = testData;
            string pathToSourceFile = Path.Combine(WorkingDirectory, "SmallTestDataFile.txt");
            await File.WriteAllLinesAsync(pathToSourceFile, unsortedArray);


            var info = new FileInfo(pathToSourceFile);

            await service.SortAsync(info.Length / 4, pathToSourceFile);

            var result = await File.ReadAllLinesAsync(pathToSourceFile);
            for (var i = 0; i < expectedResult.Count; i++)
            {
                Assert.AreEqual(expectedResult[i], result[i]);
            }
        }



        [TestCase(4, HundredMbFile)]
        [TestCase(8, HundredMbFile)]
        [TestCase(16, HundredMbFile)]
        [TestCase(32, HundredMbFile)]
        [TestCase(64, HundredMbFile)]
        [Explicit]
        [Description("Should sort 128 mb. Different File size performance")]
        public async Task PerformanceTestOfHundredMb(long chunkSizeMb, string fileName)
        {
            var path = CopyLargeArrays(fileName);
            long fileSize = chunkSizeMb * 1024 * 1024;
            await service.SortAsync(fileSize, path);
        }



        [TestCase(4, OneGbFile)] // 398
        [TestCase(16, OneGbFile)] // 394
        [TestCase(32, OneGbFile)] // 437
        [TestCase(64, OneGbFile)] // 477
        [TestCase(128, OneGbFile)]//465
        [Explicit]
        [Description("Should sort 1 GB. Different File size performance")]
        public async Task PerformanceTestOfOneGb(long chunkSizeMb, string fileName)
        {
            var path = CopyLargeArrays(fileName);
            long fileSize = chunkSizeMb * 1024 * 1024;
            await service.SortAsync(fileSize, path);

            Thread.Sleep(1000);
        }

        [TestCase(32, HundredMbFile)]
        [Explicit]
        [Description("Should check consistency")]
        public async Task Consistency_True(long chunkSizeMb, string fileName)
        {
            var path = CopyLargeArrays(fileName);
            var expected = (await File.ReadAllLinesAsync(path)).ToList();
            
            long fileSize = chunkSizeMb * 1024 * 1024;
            await service.SortAsync(fileSize, path);

            expected = expected.OrderBy(x => x, new TextThenNumberComparer()).ToList();
            var result = await File.ReadAllLinesAsync(path);

            for (int i = 0; i < expected.Count; i++)
            {
                Assert.AreEqual(expected[i], result[i]);
            }
        }


        [Test]
        [Explicit]
        [Description("Should check consistency for small file")]
        public async Task ConsistencySmallFile_True()
        {
            IComparer<string> comparer = new TextThenNumberComparer();
            var testData = TestData.RandomStrings;

            var expectedResult = testData.OrderBy(x => x, comparer).ToList();

            var unsortedArray = testData;
            string pathToSourceFile = Path.Combine(WorkingDirectory, "SmallTestDataFile.txt");
            await File.WriteAllLinesAsync(pathToSourceFile, unsortedArray);

            long maxFileSize = 1 * 1024 * 1024;
            await service.SortAsync(maxFileSize, pathToSourceFile);

            var result = await File.ReadAllLinesAsync(pathToSourceFile);
            for (var i = 0; i < expectedResult.Count; i++)
            {
                Assert.AreEqual(expectedResult[i], result[i]);
            }
        }
    }
}
