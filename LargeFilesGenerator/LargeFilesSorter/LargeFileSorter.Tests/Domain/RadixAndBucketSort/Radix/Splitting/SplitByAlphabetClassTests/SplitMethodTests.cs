﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using LargeFileSorter.Domain.RadixAndBucketSort.Radix.Splitting;
using NUnit.Framework;

namespace LargeFileSorter.Tests.Domain.RadixAndBucketSort.Radix.Splitting.SplitByAlphabetClassTests
{

    [TestFixture]
    public class SplitMethodTests : TestsBase
    {
        readonly SplitByAlphabet service = new SplitByAlphabet();

        [OneTimeSetUp]
        public override void OneTime()
        {
            base.OneTime();
            WarmingUp().Wait();

        }

        [TestCase(1 * 1024 * 1024)]
        [Description("Should split file no more then providedSize")]
        public async Task RegularArray_Sorted(long maxFileSize)
        {
            var testDataFilePath = CopyLargeArrays(HundredMbFile);


            var result = await service.SplitFiles(testDataFilePath, maxFileSize);


            var allFiles = Directory.GetFiles(Path.GetDirectoryName(testDataFilePath));

            foreach (var file in allFiles)
            {
                FileInfo nextFile = new FileInfo(file);
                Assert.LessOrEqual(nextFile.Length, maxFileSize);
            }

            for (int i = 0; i < allFiles.Length; i++)
            {
                Assert.AreEqual(result[i], allFiles[i]);
            }

        }


        [TestCase(1 * 1024 * 1024, HundredMbFile)] //23
        [TestCase(4 * 1024 * 1024, HundredMbFile)] //13
        [TestCase(8 * 1024 * 1024, HundredMbFile)] // 11
        [TestCase(10 * 1024 * 1024, OneGbFile)] // 202
        [TestCase(48 * 1024 * 1024, OneGbFile)]  //123
        [TestCase(84 * 1024 * 1024, OneGbFile)] //111
        [TestCase(164 * 1024 * 1024, OneGbFile)] // 27
        [Explicit]
        [Description("Should split file no more then providedSize")]
        public async Task PerformanceTest(long maxFileSize, string fileName)
        {
     

            var testDataFilePath = CopyLargeArrays(fileName);


            var start = DateTime.Now;
            var result = await service.SplitFiles(testDataFilePath, maxFileSize);
            var end = DateTime.Now - start;

            Console.WriteLine($"Split time: {end.TotalSeconds}");

            var allFiles = Directory.GetFiles(Path.GetDirectoryName(testDataFilePath));

            foreach (var file in allFiles)
            {
                FileInfo nextFile = new FileInfo(file);
                Assert.LessOrEqual(nextFile.Length, maxFileSize);
            }

            for (int i = 0; i < allFiles.Length; i++)
            {
                Assert.AreEqual(result[i], allFiles[i]);
            }

        }
        

        private async Task WarmingUp()
        {
            string testDataFilePath = Path.Combine(WorkingDirectory, "SameValues.txt");
            await GenerateFileWithTheSameValues(testDataFilePath, 1024 * 1024);
            await service.SplitFiles(testDataFilePath, 256 * 1024);
        }


        [TestCase(10 * 1024)]
        [Description("Should not file with duplicates")]
        public async Task ArrayWithOnlyTheSameValues_Sorted(long fileLength)
        {
            string testDataFilePath = Path.Combine(WorkingDirectory, "SameValues.txt");
            long maxFileSize = fileLength / 10;
            await GenerateFileWithTheSameValues(testDataFilePath, fileLength);


            var result = await service.SplitFiles(testDataFilePath, maxFileSize);


            var allFiles = Directory.GetFiles(Path.GetDirectoryName(testDataFilePath));

            Assert.AreEqual(1, allFiles.Length);

        }

        private static async Task GenerateFileWithTheSameValues(string testDataFilePath, long fileLength)
        {
            long totalLength = 0;
            IList<string> lines = new List<string>();
            while (totalLength < fileLength)
            {
                string line1 = "1. Black Bit";
                string line2 = "21. Black Bit";

                totalLength += line1.Length + line2.Length;
                lines.Add(line1);
                lines.Add(line2);
            }

            await File.WriteAllLinesAsync(testDataFilePath, lines);
        }
    }
}
