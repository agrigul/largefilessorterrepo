﻿using System.Linq;
using LargeFileSorter.Domain.MergeSort;
using LargeFileSorter.Domain.RadixAndBucketSort.Radix;
using NUnit.Framework;

namespace LargeFileSorter.Tests.Domain.RadixAndBucketSort.Radix.RadixSortAlgorithmClassTests
{
    [TestFixture]
    public class SortMethodTests :TestsBase
    {
        RadixSortAlgorithm service = new RadixSortAlgorithm();
       
        

        [Test]
        [Description("Should sort duplicated strings by number")]
        public void DuplicatedStrings_SortedByNumber()
        {
            var testData = TestData.DuplicatedStrings;

            var expectedResult = testData.OrderBy(x => x, new TextThenNumberComparer()).ToList();

            var unsortedArray = testData.Select(x => new RadixSortingRecord(x));

           
            var result = service.SortMsd(unsortedArray.ToList());

            for (var i = 0; i < expectedResult.Count; i++)
            {
                Assert.AreEqual(expectedResult[i], result[i].OriginalValue);
            }
        }


        [Test]
        [Description("Should sort real case strings")]
        public void RealCase_SortedByStringAndThenByNumber()
        {
            var testData = TestData.RandomStrings;

            var expectedResult = testData.OrderBy(x => x, new TextThenNumberComparer()).ToList();

            var unsortedArray = testData.Select(x => new RadixSortingRecord(x));

           
            var result = service.SortMsd(unsortedArray.ToList());

            for (var i = 0; i < expectedResult.Count; i++)
            {
                Assert.AreEqual(expectedResult[i], result[i].OriginalValue);
            }
        }
        


    }
}
