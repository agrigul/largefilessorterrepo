﻿using System.Collections.Generic;

namespace LargeFileSorter.Tests.Domain.RadixAndBucketSort
{
    public class TestData
    {
        
        public static List<string> DescSortedStrings = new List<string>
        {
            "11. K",
            "10. J",
            "9. I",
            "8. H",
            "7. G",
            "6. F",
            "5. E",
            "5. E",
            "5. E",
            "4. D",
            "3. C",
            "2. B",
            "1. A",
        };


        public static List<string> DescSortedStringsWithDots = new List<string>
        {
            "1. KKK",
            "1. KK",
            "1. J",
            "9. I",
            "8. H",
            "7. G",
            "6. F",
            "5. E",
            "5. EE",
            "5. EEE",
            "6. F",
            "4. D",
            "3. C",
            "2. B",
            "1. A",
        };


        
        public static List<string> DescSortedStringsWithoutDotsAndBackwardFormatted = new List<string>
        {
            "01 KKK",
            "01  KK",
            "01   J",
            "09   I",
            "08   H",
            "07   G",
            "06   F",
            "05   E",
            "05  EE",
            "05 EEE",
            "06   F",
            "04   D",
            "03   C",
            "02   B",
            "21   A",
            "12   A",
            "01   A",
            "01  AA",
        };

        public static IList<string> RandomStrings = new List<string>
        {
            "2. Abcd",
            "3. Abd",
            "1. Ij",
            "2. Abcd",
            "3. Abd",
            "58. Bcd efg",
            "3. Bcd efg",
            "59. Cde fgh",
            "345. Abcd",
            "3. Bcd efg",
            "3. Bcd efg",
            "3. Bcd efg",

        };


        public static IList<string> DuplicatedStrings = new List<string>
        {
            "4. Abcd",
            "2. Abcd",
            "34. Abcd",
            "1. Bcd efg",
            "12. Bcd efg",
            "21. Bcd efg",

        };

        public static IList<string> ExpectedRandomStrings = new List<string>
        {
            "2. Abcd",
            "2. Abcd",
            "345. Abcd",
            "3. Abd",
            "3. Abd",
            "3. Bcd efg",
            "3. Bcd efg",
            "3. Bcd efg",
            "58. Bcd efg",
            "1. Ij",
            "59. Cde fgh",
        };
    }
}
