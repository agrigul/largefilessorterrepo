﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LargeFilesGenerator.Domain;
using NUnit.Framework;

namespace LargeFileSorter.Tests.Domain.RadixAndBucketSort
{
    public class TestsBase
    {
        protected readonly string TestDataDirectory = Path.Combine(Directory.GetCurrentDirectory(), "TestFiles", "PreparedTestData");
        protected readonly string WorkingDirectory = Path.Combine(Directory.GetCurrentDirectory(), "TestFiles", "WorkingDirectory");
        protected const string OneGbFile = "OneGbFile.txt";
        protected const string HundredMbFile = "HundredMbFile.txt";

        [OneTimeSetUp]
        public virtual void OneTime()
        {
            PrepareTestData();
        }

        [SetUp]
        public virtual void Setup()
        {
            foreach (string file in Directory.GetFiles(WorkingDirectory))
            {
                File.Delete(file);
            }

        }


        protected void PrepareTestData()
        {
            if (Directory.Exists(TestDataDirectory) == false)
            {
                Directory.CreateDirectory(TestDataDirectory);
            }

            if (Directory.Exists(WorkingDirectory) == false)
            {
                Directory.CreateDirectory(WorkingDirectory);
            }

            var files = Directory.GetFiles(TestDataDirectory);

            IList<Task> tasks = new List<Task>();
            LargeFileFactory fileFactory = new LargeFileFactory();
            var gbFile = files.FirstOrDefault(x => x.Contains(OneGbFile));
            if (gbFile == null)
            {
                var path = Path.Combine(TestDataDirectory, OneGbFile);
                long fileSize = 1 * 1024 * 1024 * 1024;

                var task = Task.Run(() => fileFactory.GenerateDataAndWriteToFileAsync(path, fileSize));
                tasks.Add(task);
            }

            var hundredMbFile = files.FirstOrDefault(x => x.Contains(HundredMbFile));
            if (hundredMbFile == null)
            {
                var path = Path.Combine(TestDataDirectory, HundredMbFile);
                long fileSize = 100 * 1024 * 1024;

                var task = Task.Run(() => fileFactory.GenerateDataAndWriteToFileAsync(path, fileSize));
                tasks.Add(task);
            }

            Task.WaitAll(tasks.ToArray());
        }

        protected string CopyLargeArrays(string fileName)
        {
            var destPath = Path.Combine(WorkingDirectory, fileName);
            File.Copy(Path.Combine(TestDataDirectory, fileName), destPath);
            return destPath;
        }

    }
}