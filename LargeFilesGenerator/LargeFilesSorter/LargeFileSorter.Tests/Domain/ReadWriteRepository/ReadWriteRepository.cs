﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LargeFileSorter.Tests.Domain.ReadWriteRepository
{
    public class ReadWriteRepository
    {
        public ICollection<string> ReadViaStreamLineByLineToList(string path, bool detectEncoding = true,
            int buffSize = 4096)
        {
            IList<string> collection = new List<string>();

            using (var reader = new StreamReader(path, Encoding.Default, detectEncoding, buffSize))
            {
                while (reader.EndOfStream == false)
                {
                    var line = reader.ReadLine();
                    collection.Add(line);
                }
            }

            return collection;
        }


        public ICollection<string> ReadBytesArray(string path)
        {

            var result = File.ReadAllBytes(path);
            var collection = BytesToStringList(result);

            return collection;
        }


        public ICollection<string> ReadViaStreamAllText(string path, bool detectEncoding = true, int buffSize = 4096)
        {
            IList<string> collection = new List<string>();

            using (var reader = new StreamReader(path, Encoding.Default, detectEncoding, buffSize))
            {
                var result = reader.ReadToEndAsync().ConfigureAwait(false).GetAwaiter().GetResult();
                collection = result.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            }

            return collection;
        }





        public Queue<string> ReadViaStreamLineByLineToQueue(string path, bool detectEncoding = true,
            int buffSize = 4096)
        {
            var collection = new Queue<string>();

            using (var reader = new StreamReader(path, Encoding.Default, detectEncoding, buffSize))
            {
                while (reader.EndOfStream == false)
                {
                    var line = reader.ReadLine();
                    collection.Enqueue(line);
                }
            }

            return collection;
        }


        public ICollection<string> ReadViaFileStreamAndReader(string path, int buffSize)
        {
            var collection = new List<string>();
            using (FileStream fileStream = File.OpenRead(path))
            using (var reader = new StreamReader(fileStream))
            {
                while (reader.EndOfStream == false)
                {
                    var line = reader.ReadLine();
                    collection.Add(line);
                }
            }

            return collection;
        }


        public ICollection<string> ReadViaFileReadAllLines(string path)
        {
            var collection = File.ReadAllLines(path, Encoding.Default);
            return collection;
        }




        public static List<string> BytesToStringList(byte[] bytes)
        {
            var collection = new List<string>();

            Stream stream = new MemoryStream(bytes);
            using (StreamReader reader = new StreamReader(stream, Encoding.Default, false, 1024 * 1024))
            {
                while (reader.EndOfStream == false)
                {
                    var line = reader.ReadLine();
                    collection.Add(line);
                }
            }

            return collection;
        }

        public void WriteViaStreamLineByLineSingleThread(string path, ICollection<string> collection, bool append,
            int buffSize)
        {
            using (var writer = new StreamWriter(path, append, Encoding.Default, buffSize))
            {
                foreach (var line in collection)
                {
                    writer.WriteLine(line);
                }
            }
        }


        public void WriteViaWriteAllLines(string path, ICollection<string> collection)
        {
            File.WriteAllLines(path, collection, Encoding.Default);
        }


        public void WriteViaStringBuilder(string path, ICollection<string> collection, int buffSize)
        {
            StringBuilder builder = new StringBuilder(collection.Count);
            foreach (string line in collection)
            {
                builder.AppendLine(line);
            }

            using (var writer = new StreamWriter(path, true, Encoding.Default, buffSize))
            {
                writer.Write(builder);
            }
        }


        public void WriteViaBytesArray(string path, ICollection<string> collection, int buffSize)
        {
            byte[] bytes;
            using (MemoryStream stream = new MemoryStream())
            {
                foreach (var line in collection)
                {
                    var bytesLine = Encoding.Default.GetBytes(line);
                    stream.Write(bytesLine);
                }

                bytes = stream.ToArray();
            }

            using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                fs.Write(bytes, 0, bytes.Length);
            }
        }
    }
}
