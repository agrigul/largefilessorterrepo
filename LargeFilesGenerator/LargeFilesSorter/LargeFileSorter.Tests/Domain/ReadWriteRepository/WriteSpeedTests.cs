﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LargeFilesGenerator.Domain;
using NUnit.Framework;

namespace LargeFileSorter.Tests.Domain.ReadWriteRepository
{
    [TestFixture]
    public class WriteSpeedTests
    {
        private readonly string currentDirectory = Path.Combine(Directory.GetCurrentDirectory(), "TestFiles", "ReadWrite");
        public const string OneGBFile = "OneGbFile.txt";
        public const string HundredMBFile = "HundredMbFile.txt";

        readonly LargeFileFactory fileFactory = new LargeFileFactory();
        public ReadWriteRepository ReadWriteRepository = new ReadWriteRepository();

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            if (Directory.Exists(currentDirectory) == false)
            {
                Directory.CreateDirectory(currentDirectory);
            }

            var files = Directory.GetFiles(currentDirectory);

            IList<Task> tasks = new List<Task>();

            var gbFile = files.FirstOrDefault(x => x.Contains(OneGBFile));
            if (gbFile == null)
            {
                var path = Path.Combine(currentDirectory, OneGBFile);
                long fileSize = 1 * 1024 * 1024 * 1024;

                var task = Task.Run(() => fileFactory.GenerateDataAndWriteToFileAsync(path, fileSize));
                tasks.Add(task);
            }

            var hundredMbFile = files.FirstOrDefault(x => x.Contains(HundredMBFile));
            if (hundredMbFile == null)
            {
                var path = Path.Combine(currentDirectory, HundredMBFile);
                long fileSize = 100 * 1024 * 1024;

                var task = Task.Run(() => fileFactory.GenerateDataAndWriteToFileAsync(path, fileSize));
                tasks.Add(task);
            }

            Task.WaitAll(tasks.ToArray());

        }

        [SetUp]
        public void Setup()
        {

            var files = Directory.GetFiles(currentDirectory);

            foreach (var file in files)
            {
                if (file.Contains(OneGBFile) == false &&
                    file.Contains(HundredMBFile) == false)
                {
                    File.Delete(file);
                }
            }

        }

        #region read tests
        [TestCase(OneGBFile)]  // better 11
        [TestCase(HundredMBFile)]
        [Explicit]
        [Description("Read via stream line by line to list")]
        public void ReadSpeedViaStreamLineByLineToList(string fileName)
        {
            string path = Path.Combine(currentDirectory, fileName);

            var start = DateTime.Now;
            var result = ReadWriteRepository.ReadViaStreamLineByLineToList(path);
            var readTime = DateTime.Now - start;

            Assert.IsTrue(result.Count > 100);
            Console.WriteLine($"Read file via stream line by line to list: {readTime.TotalSeconds}, file {fileName}");


        }

        [TestCase(OneGBFile)]  // 12.52
        [TestCase(HundredMBFile)] // better
        [Explicit]
        [Description("Read via stream line by line to queue")]
        public void ReadSpeedViaStreamLineByLineToQueue(string fileName)
        {
            string path = Path.Combine(currentDirectory, fileName);

            var start = DateTime.Now;
            ReadWriteRepository.ReadViaStreamLineByLineToQueue(path);
            var readTime = DateTime.Now - start;

            Console.WriteLine($"Read via stream line by line to queue: {readTime.TotalSeconds}, file {fileName}");
        }


        [TestCase(OneGBFile, true)] // best 11
        [TestCase(OneGBFile, false)]
        [Explicit]
        [Description("Read via stream line by line with encoding detection")]
        public void ReadWithEncoding(string fileName, bool encoding)
        {
            string path = Path.Combine(currentDirectory, fileName);

            var start = DateTime.Now;
            var result = ReadWriteRepository.ReadViaStreamLineByLineToList(path, encoding);
            var readTime = DateTime.Now - start;

            Assert.IsTrue(result.Count > 100);
            Console.WriteLine($"Read file via stream line by line to list: {readTime.TotalSeconds}, encoding {encoding}");
        }

        [TestCase(OneGBFile, 1024 * 4)]
        [TestCase(OneGBFile, 1024 * 32)]
        [TestCase(OneGBFile, 1024 * 64)]
        [TestCase(OneGBFile, 1024 * 128)]
        [TestCase(OneGBFile, 1024 * 256)]
        [TestCase(OneGBFile, 1024 * 512)] // better
        [TestCase(OneGBFile, 1024 * 1024)] // better 10.22
        [Explicit]
        [Description("Read via stream line by line with encoding detection")]
        public void ReadWithBuffSize(string fileName, int buffSize)
        {
            string path = Path.Combine(currentDirectory, fileName);

            var start = DateTime.Now;
            var result = ReadWriteRepository.ReadViaStreamLineByLineToList(path, buffSize: buffSize);
            var readTime = DateTime.Now - start;

            Assert.IsTrue(result.Count > 100);
            Console.WriteLine($"Read file via stream line by line to list: {readTime.TotalSeconds}, buffSize {buffSize}");


        }


        [TestCase(OneGBFile)] // 14
        [TestCase(HundredMBFile)]
        [Explicit]
        [Description("Read via File.ReadAllLines")]
        public void ReadSpeedViaReadAllLines(string fileName)
        {
            string path = Path.Combine(currentDirectory, fileName);

            var start = DateTime.Now;
            var result = ReadWriteRepository.ReadViaFileReadAllLines(path);
            var readTime = DateTime.Now - start;

            Assert.IsTrue(result.Count > 100);
            Console.WriteLine($"Read via File.ReadAllLines: {readTime.TotalSeconds}, file {fileName}");


        }


        [TestCase(OneGBFile)]
        [TestCase(HundredMBFile)]
        [Explicit]
        [Description("Read all text and then split")]
        public void ReadAllTextAndThenSplitInMemory(string fileName)
        {
            string path = Path.Combine(currentDirectory, fileName);

            var start = DateTime.Now;
            var result = ReadWriteRepository.ReadViaStreamAllText(path);
            var readTime = DateTime.Now - start;

            Assert.IsTrue(result.Count > 100);
            Assert.IsTrue(result.Take(100).All(x => string.IsNullOrWhiteSpace(x) == false));
            Console.WriteLine($"Read all text and then split: {readTime.TotalSeconds}, file {fileName}");


        }


        [TestCase(OneGBFile)] // one of the best 9.54
        [TestCase(HundredMBFile)] // one of the best
        [Explicit]
        [Description("Read all text as bytes array and then split")]
        public void ReadTextAsBytesArrayToMemoryAndThenSplit(string fileName)
        {
            string path = Path.Combine(currentDirectory, fileName);

            var start = DateTime.Now;
            var result = ReadWriteRepository.ReadBytesArray(path);
            var readTime = DateTime.Now - start;

            Assert.IsTrue(result.Count > 100);
            Assert.IsTrue(result.Take(100).All(x => string.IsNullOrWhiteSpace(x) == false));
            Console.WriteLine($"Read all text and then split: {readTime.TotalSeconds}, file {fileName}");
        }


        [TestCase(OneGBFile)]  // best
        [TestCase(HundredMBFile)] // best  8.9
        [Explicit]
        [Description("Read via stream line by line to list not encoding and big buffer")]
        public void ReadSpeedViaStreamLineByLineNoEncodingAndBigBuffer(string fileName)
        {
            string path = Path.Combine(currentDirectory, fileName);

            var start = DateTime.Now;
            var result = ReadWriteRepository.ReadViaStreamLineByLineToList(path, false, 1024 * 1024);
            var readTime = DateTime.Now - start;

            Assert.IsTrue(result.Count > 100);
            Console.WriteLine($"Read file via stream line by line to list: {readTime.TotalSeconds}, file {fileName}");


        }

        [TestCase(OneGBFile, 8 * 1024 * 1204)]//8.6
        [TestCase(OneGBFile, 1024 * 1204)]//13
        [TestCase(OneGBFile, 1024)]//17
        [TestCase(HundredMBFile, 1024 * 1204)]//1.68
        [TestCase(HundredMBFile, 1024)]//1.44
        [Explicit]
        [Description("Read via File.OpenRead")]
        public void ReadViaFileStreamAndReader(string fileName, int bufferSize)
        {
            string path = Path.Combine(currentDirectory, fileName);

            var start = DateTime.Now;
            var result = ReadWriteRepository.ReadViaFileStreamAndReader(path, bufferSize);
            var readTime = DateTime.Now - start;

            Assert.IsTrue(result.Count > 100);
            Console.WriteLine($"Read file via stream line by line to list: {readTime.TotalSeconds}, file {fileName}");


        }

        #endregion region read tests


        #region  write tests

        [TestCase(OneGBFile)] // 36
        [TestCase(HundredMBFile)]
        [Explicit]
        [Description("Write via stream line by line single file")]
        public void WriteSpeedViaStreamLineByLine(string fileName)
        {
            string readFilePath = Path.Combine(currentDirectory, fileName);
            var collection = ReadWriteRepository.ReadBytesArray(readFilePath);

            string writeFilePath = Path.Combine(currentDirectory, "Write.txt");
            var start = DateTime.Now;
            ReadWriteRepository.WriteViaStreamLineByLineSingleThread(writeFilePath, collection, true, 4096);
            var readTime = DateTime.Now - start;

            Console.WriteLine($"Write via stream line by line single file: {readTime.TotalSeconds}, file {fileName}");
        }



        [TestCase(OneGBFile, true)] // better 33.6
        [TestCase(OneGBFile, false)]
        [Explicit]
        [Description("Write via stream line by line with append")]
        public void WriteSpeedViaStreamLineByLineAppend(string fileName, bool append)
        {
            string readFilePath = Path.Combine(currentDirectory, fileName);
            var collection = ReadWriteRepository.ReadBytesArray(readFilePath);
            string writeFilePath = Path.Combine(currentDirectory, "Write.txt");

            var start = DateTime.Now;
            ReadWriteRepository.WriteViaStreamLineByLineSingleThread(writeFilePath, collection, append, 4096);
            var readTime = DateTime.Now - start;

            Console.WriteLine($"Write via stream line by line with append: {readTime.TotalSeconds}, append {append}");
        }



        [TestCase(OneGBFile, 1024 * 4)]
        [TestCase(OneGBFile, 1024 * 64)]
        [TestCase(OneGBFile, 1024 * 128)]
        [TestCase(OneGBFile, 1024 * 256)]
        [TestCase(OneGBFile, 1024 * 512)]
        [TestCase(OneGBFile, 1024 * 1024)] // best 1.83
        [TestCase(OneGBFile, 1024 * 2048)] // better 1.99
        [TestCase(OneGBFile, 1024 * 1024 * 50)] // better 1.99
        [Explicit]
        [Description("Write via stream line by line single file buffer size")]
        public void WriteSpeedViaStreamLineByLineBuffer(string fileName, int bufferSize)
        {
            string readFilePath = Path.Combine(currentDirectory, fileName);
            var collection = ReadWriteRepository.ReadBytesArray(readFilePath);
            string writeFilePath = Path.Combine(currentDirectory, "Write.txt");

            var start = DateTime.Now;
            ReadWriteRepository.WriteViaStreamLineByLineSingleThread(writeFilePath, collection, true, bufferSize);
            var readTime = DateTime.Now - start;

            Console.WriteLine($"Write via stream line by line single file buffer size: {readTime.TotalSeconds}, bufferSize {bufferSize}");
        }


        [TestCase(OneGBFile, 1024 * 64)]
        [TestCase(OneGBFile, 1024 * 128)]
        [TestCase(OneGBFile, 1024 * 256)] // 5.4
        [TestCase(OneGBFile, 1024 * 512)]
        [TestCase(OneGBFile, 1024 * 1024)] // 5.4
        [TestCase(OneGBFile, 1024 * 2048)]
        [Explicit]
        [Description("Write via string builder buffer size")]
        public void WriteStringBuilderBuffer(string fileName, int bufferSize)
        {
            string readFilePath = Path.Combine(currentDirectory, fileName);
            var collection = ReadWriteRepository.ReadBytesArray(readFilePath);
            string writeFilePath = Path.Combine(currentDirectory, "Write.txt");

            var start = DateTime.Now;
            ReadWriteRepository.WriteViaStringBuilder(writeFilePath, collection, bufferSize);
            var readTime = DateTime.Now - start;

            Console.WriteLine($"Write via string builder buffer size: {readTime.TotalSeconds}, bufferSize {bufferSize}");
        }



        [TestCase(OneGBFile, 1024 * 64)]
        [TestCase(OneGBFile, 1024 * 128)] // 7.4
        [TestCase(OneGBFile, 1024 * 256)]
        [TestCase(OneGBFile, 1024 * 512)]
        [TestCase(OneGBFile, 1024 * 1024)]
        [TestCase(OneGBFile, 1024 * 2048)]
        [Explicit]
        [Description("Write via array of bytes buffer size")]
        public void WriteBytesArrayBuffer(string fileName, int bufferSize)
        {
            string readFilePath = Path.Combine(currentDirectory, fileName);
            var collection = ReadWriteRepository.ReadBytesArray(readFilePath);
            string writeFilePath = Path.Combine(currentDirectory, "Write.txt");

            var start = DateTime.Now;
            ReadWriteRepository.WriteViaBytesArray(writeFilePath, collection, bufferSize);
            var readTime = DateTime.Now - start;

            Console.WriteLine($"Write via array of bytes buffer size: {readTime.TotalSeconds}, bufferSize {bufferSize}");
        }



        [TestCase(HundredMBFile)] // bad
        [TestCase(OneGBFile)] // bad 35
        [Explicit]
        [Description("Write via write all files")]
        public void WriteWithFileAndWriteAllLines(string fileName)
        {
            string readFilePath = Path.Combine(currentDirectory, fileName);
            var collection = ReadWriteRepository.ReadBytesArray(readFilePath);
            string writeFilePath = Path.Combine(currentDirectory, "Write.txt");

            var start = DateTime.Now;
            ReadWriteRepository.WriteViaWriteAllLines(writeFilePath, collection);
            var writeTime = DateTime.Now - start;

            Console.WriteLine($"Write via write all files: {writeTime.TotalSeconds}");
        }

        [TestCase(HundredMBFile)]
        [TestCase(OneGBFile)] // 11.27
        [Explicit]
        [Description("The Best Read and Write Combination")]
        public void TheBestReadWriteCombination(string fileName)
        {
            string readFilePath = Path.Combine(currentDirectory, fileName);

            var start = DateTime.Now;
            var collection = ReadWriteRepository.ReadViaStreamLineByLineToList(readFilePath, true, 1024 * 1024);
            var readTime = DateTime.Now - start;
            Console.WriteLine($"Read the best way {readTime.TotalSeconds}, file {fileName}");

            string writeFilePath = Path.Combine(currentDirectory, "Write.txt");

            start = DateTime.Now;
            ReadWriteRepository.WriteViaStreamLineByLineSingleThread(writeFilePath, collection, true, 1024 * 1024);
            var writeime = DateTime.Now - start;

            Console.WriteLine($"Write the best way {writeime.TotalSeconds}, file {fileName}");
            Console.WriteLine($"Total: {readTime.TotalSeconds + writeime.TotalSeconds}");
        }

        #endregion  write tests
    }


}
