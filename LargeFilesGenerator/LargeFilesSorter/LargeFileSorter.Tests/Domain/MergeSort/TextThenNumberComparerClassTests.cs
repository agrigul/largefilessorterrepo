﻿using System;
using System.Collections.Generic;
using System.Linq;
using LargeFileSorter.Domain.MergeSort;
using NUnit.Framework;

namespace LargeFileSorter.Tests.Domain.MergeSort
{
    [TestFixture]
    public class TextThenNumberComparerClassTests
    {

        [TestCase("1. A", "1. A", 0)]
        [TestCase("1. A", "1. B", -1)]
        [TestCase("1. B", "1. A", 1)]
        [TestCase("1. A", "2. A", -1)]
        [TestCase("1. A", "2. A", -1)]
        [TestCase("2. A", "1. A", 1)]
        [TestCase("20. A", "10. A", 1)]
        [TestCase("10. A", "20. A", -1)]
        [TestCase("20. A", "20. A", 0)]
        [TestCase("1. A", "1. AA", -1)]
        [TestCase("1. AA", "1. A", 1)]
        [TestCase("1. AA", "1. AA", 0)]
        [TestCase("1. Aa", "1. AA", 1)]
        [TestCase("1. AA", "1. Aa", -1)]
        [TestCase("1. AA", "1. Ab", -1)]
        [TestCase("1. Ab", "1. AA", 1)]
        [TestCase("1. Ab", "1. AAA", 1)]
        [TestCase(null, null, 0)]
        [TestCase("", "", 0)]
        [Description("Should compare 2 strings of special format")]
        public void ValidParams_CompareResult(string x, string y, int expectedResult)
        {

            var comparer = new TextThenNumberComparer();

            var result = comparer.Compare(x, y);
            Assert.AreEqual(expectedResult, result);
        }
        
        [TestCase("1. A", "")]
        [TestCase("", "1. AAA")]
        [TestCase(null, "1. AAA")]
        [TestCase("1. A", null)]
        [TestCase("A. 1", "1. A")]
        [TestCase("1. A", "A. 1")]
        [TestCase(". A", "1. A")]
        [TestCase("1. A", ". A")]
        [TestCase("1 A", ". A")]
        [TestCase("1. A", "1 A")]
        [TestCase("1. ", "1. A")]
        [TestCase("1. A", "1 ")]
        [TestCase("1.", "1. A")]
        [TestCase("1. A", "1.")]
        [TestCase("1", "1. A")]
        [TestCase("1. A", "1")]
        [Description("Should throw exception")]
        public void InvValidParams_Exception(string x, string y)
        {

            var comparer = new TextThenNumberComparer();

            Assert.Throws<FormatException>(() => comparer.Compare(x, y));

        }


        [Test]
        [Description("Should sort lines correctly")]
        public void UnsortedLines_CorrectOrder()
        {
            var notSortedList = new List<string>()
            {
                "12. Black Bitter Banana",
                "11. Black Bitter Cucumber",
                "11. Black Bitter Cucumber",
                "12. Black Bitter Banana",
                "13. Black Bitter Cucumber"
            };

            var expectedSortedList = new List<string>()
            {
                "12. Black Bitter Banana",
                "12. Black Bitter Banana",
                "11. Black Bitter Cucumber",
                "11. Black Bitter Cucumber",
                "13. Black Bitter Cucumber",
            };


            var result = notSortedList.OrderBy(x => x, new TextThenNumberComparer()).ToList();


            for (int i = 0; i < expectedSortedList.Count; i++)
            {
                Assert.AreEqual(expectedSortedList[i], result[i]);
            }
        }
    }
}
