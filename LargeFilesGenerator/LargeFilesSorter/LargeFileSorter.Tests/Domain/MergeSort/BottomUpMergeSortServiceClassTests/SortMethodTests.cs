﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LargeFilesGenerator.Domain;
using LargeFileSorter.Domain.MergeSort;
using LargeFileSorter.Tests.Domain.RadixAndBucketSort;
using NUnit.Framework;

namespace LargeFileSorter.Tests.Domain.MergeSort.BottomUpMergeSortServiceClassTests
{
    [TestFixture]
    public class SortMethodTests : TestsBase
    {
        
        string testDataFileName = "TestGeneratedFile.txt";

        readonly BottomUpMergeSortService serviceUnderTests = new BottomUpMergeSortService();
     
        

        [OneTimeSetUp]
        public virtual void OneTime()
        {
            base.OneTime();
        }



        [TestCase(1024, 1024, 4)]
        [TestCase(1024, 1024, 8)]
        [TestCase(1024, 1024, 32)]
        [TestCase(1024, 1024, 64)]
        [TestCase(1024, 1024, 8)]
        [TestCase(1024, 2048, 8)]
        [TestCase(1024, 9000, 8)]
        [Explicit]
        [Description("Should sort large text file using multithreading")]
        public async Task FileWithText_NumberOfThreadsTest(long fileSizeMb, long maxRamUsageSizeMb, int numberOfThreads = 4)
        {
            await SortAlgo(fileSizeMb, maxRamUsageSizeMb, numberOfThreads);

        }


        [TestCase(1024 / 2, 16, 3)]
        [TestCase(128, 16, 3)]
        [TestCase(128, 1024, 3)]
        [TestCase(1024, 256, 3)]
        [TestCase(1024, 1024, 3)]//154
        [TestCase(1024, 2048, 6)]
        [TestCase(1024, 2048, 32)]
        [TestCase(1024, 2048, 16)]
        [TestCase(1024, 4096, 3)]
        [Explicit]
        [Description("Should sort large text file using different RAM size")]
        public async Task FileWithText_SizeOfMemoryTest(long fileSizeMb, long maxRamUsageSizeMb, int numberOfThreads = 4)
        {
            await SortAlgo(fileSizeMb, maxRamUsageSizeMb, numberOfThreads);

        }


        public async Task SortAlgo(long fileSizeMb, long maxRamUsageSizeMb, int numberOfThreads = 4)
        {

            string path = Path.Combine(WorkingDirectory, testDataFileName);


            if (fileSizeMb == 1024)
            {
                path = CopyLargeArrays(OneGbFile);
            }
            else
            if (fileSizeMb == 128)
            {
                path = CopyLargeArrays(HundredMbFile);
            }
            else
            {
                fileSizeMb = fileSizeMb * 1024 * 1024;
                var fileFactory = new LargeFileFactory();

                await fileFactory.GenerateDataAndWriteToFileAsync(path, fileSizeMb)
                    .ConfigureAwait(false);
            }


            maxRamUsageSizeMb = maxRamUsageSizeMb * 1024 * 1024;


            long fileSizeBeforeSort = new FileInfo(path).Length;

            var start = DateTime.Now;
            await serviceUnderTests.SortAsync(path, maxRamUsageSizeMb, numberOfThreads);

            Console.WriteLine($"Total used time: {(DateTime.Now - start).TotalSeconds}");
            long fileSizeAfterSort = new FileInfo(path).Length;


            Assert.AreEqual(fileSizeBeforeSort, fileSizeAfterSort);


        }



        [TestCase]
        [Description("Should sort small text file")]
        public async Task SmallFileRandomData_Sorted()
        {
            int maxRamUsageSizeMb = 1024;
            var testStrings = TestData.RandomStrings;
            string path = Path.Combine(WorkingDirectory, testDataFileName);

            await File.WriteAllLinesAsync(path, TestData.RandomStrings);


            await serviceUnderTests.SortAsync(path, maxRamUsageSizeMb, 3);


            var allLinesAfter = (await File.ReadAllLinesAsync(path)).ToList();
            var expected = testStrings.OrderBy(x => x, new TextThenNumberComparer()).ToList().ToList();
            for (int i = 0; i < allLinesAfter.Count; i++)
            {
                Assert.AreEqual(expected[i], allLinesAfter[i]);
            }

            Console.WriteLine("The sorting order was checked");


        }
    }
}
