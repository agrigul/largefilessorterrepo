﻿using System.Collections.Generic;
using System.Linq;
using LargeFileSorter.Domain.BinaryTree;
using LargeFileSorter.Domain.MergeSort;
using LargeFileSorter.Domain.MergeSort.InMemorySort;
using NUnit.Framework;

namespace LargeFileSorter.Tests.Domain.MergeSort.BinaryTreeClassTests
{
    [TestFixture]
    public class AddMethodTests
    {

        [Test]
        [Description("Should add new nodes as sorted")]
        public void Add_Sorted()
        {
            BinaryTreeNode<SortingRecord> tree = null;

            foreach (var value in descSortedStrings)
            {
                var nextNode = new SortingRecord(value);
                if (tree == null)
                {
                    tree = new BinaryTreeNode<SortingRecord>(nextNode, null);
                }
                else
                {
                    
                    tree.Add(nextNode);
                }
            }

            var expectedResult = descSortedStrings.OrderBy(x => x, new TextThenNumberComparer()).ToList();


            var listOfRecords = tree.Print().Select(x=>x.OriginalValue).ToList();

            for (int i = 0; i < expectedResult.Count; i++)
            {
                Assert.AreEqual(expectedResult[i], listOfRecords[i]);
            }

        }


        [Test]
        [Description("Should sort random values")]
        public void AddUnserted_Sorted()
        {
            BinaryTreeNode<SortingRecord> tree = null;

            foreach (var value in randomStrings)
            {
                var nextNode = new SortingRecord(value);
                if (tree == null)
                {
                    tree = new BinaryTreeNode<SortingRecord>(nextNode, null);
                }
                else
                {
                    
                    tree.Add(nextNode);
                }
            }

            var expectedResult = randomStrings.OrderBy(x => x, new TextThenNumberComparer()).ToList();


            var listOfRecords = tree.Print().Select(x=>x.OriginalValue).ToList();

            for (int i = 0; i < expectedResult.Count; i++)
            {
                Assert.AreEqual(expectedResult[i], listOfRecords[i]);
            }

        }
        

        readonly List<string> descSortedStrings = new List<string>
        {
            "11. K",
            "10. J",
            "9. I",
            "8. H",
            "7. G",
            "6. F",
            "5. E",
            "5. E",
            "5. E",
            "4. D",
            "3. C",
            "2. B",
            "1. A",
        };

        readonly IList<string> randomStrings = new List<string>
        {
            "2. Abcd",
            "3. Abd",
            "1. Ij",
            "2. Abcd",
            "3. Abd",
            "58. Bcd efg",
            "3. Bcd efg",
            "59. Cde fgh",
            "345. Abcd",
            "3. Bcd efg",
            "3. Bcd efg",
            "3. Bcd efg",

        };

        IList<string> expectedRandomStrings = new List<string>
        {
            "2. Abcd",
            "2. Abcd",
            "345. Abcd",
            "3. Abd",
            "3. Abd",
            "3. Bcd efg",
            "3. Bcd efg",
            "3. Bcd efg",
            "58. Bcd efg",
            "1. Ij",
            "59. Cde fgh",
        };

    }
}
