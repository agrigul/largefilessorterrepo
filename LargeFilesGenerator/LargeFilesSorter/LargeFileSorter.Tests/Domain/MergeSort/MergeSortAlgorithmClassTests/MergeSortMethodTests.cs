﻿using System.Collections.Generic;
using System.Linq;
using LargeFileSorter.Domain.MergeSort;
using LargeFileSorter.Domain.MergeSort.InMemorySort;
using NUnit.Framework;

namespace LargeFileSorter.Tests.Domain.MergeSort.MergeSortAlgorithmClassTests
{
    [TestFixture]
    public class MergeSortMethodTests
    {

        [Test]
        [Description("Should sort random list")]
        public void NotSortedRealStringList_SortedList()
        {
            var nonSortedRecords = randomStrings.Select(x => new SortingRecord(x));

            var sort = new MergeSortAlgorithm();
            var result = sort.MergeSort(nonSortedRecords.ToList());

            var sortedStrings = result.Select(x => x.OriginalValue).ToArray();
            var expectedResult = randomStrings.OrderBy(x => x, new TextThenNumberComparer()).ToList();
            for (var i = 0; i < expectedResult.Count; i++)
            {
                Assert.AreEqual(expectedResult[i], sortedStrings[i]);
            }
        }


        [Test]
        [Description("Should sort reverse list")]
        public void ReversedStringList_SortedList()
        {

            var nonSortedRecords = descSortedStrings.Select(x => new SortingRecord(x));

            var sort = new MergeSortAlgorithm();
            var result = sort.MergeSort(nonSortedRecords.ToList());


            var sortedStrings = result.Select(x => x.OriginalValue).ToArray();
            var expectedResult = descSortedStrings.OrderBy(x => x, new TextThenNumberComparer()).ToList();
            for (var i = 0; i < expectedResult.Count; i++)
            {
                Assert.AreEqual(expectedResult[i], sortedStrings[i]);
            }
        }


        [Test]
        [Description("Should sort reverse list")]
        public void MergeSeveralSortedQueues_SortedList()
        {

            var sort = new MergeSortAlgorithm();

            List<SortingRecord> expectedSortedList = new List<SortingRecord>();
            const int numberOfQueues = 10;
            for (int i = 0; i < numberOfQueues; i++)
            {
                var listToAdd = descSortedStrings.Select(x => new SortingRecord(x));
                expectedSortedList.AddRange(listToAdd);
            }

            expectedSortedList = sort.MergeSort(expectedSortedList);

            List<Queue<SortingRecord>> listOfQueuesToMerge = new List<Queue<SortingRecord>>();
            for (int i = 0; i < numberOfQueues; i++)
            {
                var result = sort.MergeSort(descSortedStrings);
                listOfQueuesToMerge.Add(new Queue<SortingRecord>(result));
            }


            var mergeResult = sort.MergeWithLinkedList(listOfQueuesToMerge);

            var mergedAndSortedStrings = mergeResult.Select(x => x.OriginalValue).ToArray();
            for (var i = 0; i < expectedSortedList.Count; i++)
            {
                Assert.AreEqual(expectedSortedList[i].OriginalValue, mergedAndSortedStrings[i]);
            }
        }

        

        readonly List<string> descSortedStrings = new List<string>
        {
            "11. K",
            "10. J",
            "9. I",
            "8. H",
            "7. G",
            "6. F",
            "5. E",
            "5. E",
            "5. E",
            "4. D",
            "3. C",
            "2. B",
            "1. A",
        };

        readonly IList<string> randomStrings = new List<string>
        {
            "2. Abcd",
            "3. Abd",
            "1. Ij",
            "2. Abcd",
            "3. Abd",
            "58. Bcd efg",
            "3. Bcd efg",
            "59. Cde fgh",
            "345. Abcd",
            "3. Bcd efg",
            "3. Bcd efg",
            "3. Bcd efg",

        };

        IList<string> expectedRandomStrings = new List<string>
        {
            "2. Abcd",
            "2. Abcd",
            "345. Abcd",
            "3. Abd",
            "3. Abd",
            "3. Bcd efg",
            "3. Bcd efg",
            "3. Bcd efg",
            "58. Bcd efg",
            "1. Ij",
            "59. Cde fgh",
        };

    }
}
