﻿using System;
using System.IO;
using LargeFileSorter.Domain.MergeSort;

namespace LargeFileSorter
{
    class Program
    {

        static void Main(string[] args)
        {

            try
            {

                Console.WriteLine("\nThis is a test task service made by Artem Griguletskiy for interview.\n" +
                                  "All Rights reserved. Algorithm, code or program can't be used for any purposes, but once time for interview.\n");

                if (DateTime.Now > new DateTime(2021, 9, 1))
                {
                    Console.WriteLine("Please, contact developer");
                    Console.ReadLine();
                }

                Console.WriteLine(
                    "\nPlease, enter path to sorted file. If nothing was entered the '\\bin\\TestFiles\\TestGeneratedFile.txt' file will be used\n");
                string pathToFile = null;

                while (File.Exists(pathToFile) == false)
                {
                    pathToFile = Console.ReadLine();

                    if (string.IsNullOrWhiteSpace(pathToFile))
                    {
                        pathToFile = Path.Combine(Directory.GetCurrentDirectory(), "TestFiles", "TestGeneratedFile.txt");
                    }

                    if (File.Exists(pathToFile) == false)
                    {
                        Console.WriteLine($"\nThe provided path is invalid or file was not found: {pathToFile}.\n");
                    }
                }

                FileInfo info = new FileInfo(pathToFile);
                bool allIsDone = false;
                while (allIsDone == false)
                {
                    long ramSize = 0;
                    int? numberOfThreads = null;
                    const int recommendedNumberOfFiles = 16;

                    long fileSize = info.Length;

                    long defaultRamSize = fileSize / recommendedNumberOfFiles;
                    Console.WriteLine(
                        "\nDo you have memory limits (MB)? Please enter number of MB you may allow to use or leave blank.");
                    Console.WriteLine(
                        $"If you don't enter any value, at least {defaultRamSize / 1024 / 1204} MB file may be loaded, and it may take twice more of RAM.\n");


                    bool validRamSize = false;
                    while (validRamSize == false)
                    {

                        string ramSizeAsString = Console.ReadLine();

                        if (long.TryParse(ramSizeAsString, out ramSize))
                        {
                            if (ramSize < 1)
                            {
                                Console.WriteLine("Please enter value more then 1 MB of memory");
                                continue;
                            }
                            validRamSize = true;
                            ramSize = ramSize * 1024 * 1024;
                        }
                        else
                        {
                            validRamSize = true;
                            ramSize = defaultRamSize;
                        }


                        Console.WriteLine(
                            "\nDo you want to specify number of used threads? Please enter number of threads you may allow to use or leave blank.");
                        Console.WriteLine(
                            $"If you don't enter any value, {Environment.ProcessorCount} threads will be used\n");

                        string numberOfThreadsAsString = Console.ReadLine();
                        if (int.TryParse(numberOfThreadsAsString, out int res))
                        {
                            numberOfThreads = res;
                        }
                    }

                    Console.WriteLine("Start...");
                    var service = new BottomUpMergeSortService();
                    service.SortAsync(pathToFile, ramSize, numberOfThreads).ConfigureAwait(false).GetAwaiter().GetResult();


                    Console.WriteLine("The file was sorted");
                    Console.ReadLine();

                    Console.WriteLine("\nDo you want to repeat? (y\\n)");
                    var recreate = Console.ReadLine();
                    allIsDone = recreate != "y";

                }

            }
            catch (Exception e)
            {
                Console.WriteLine($"\nThere is an exception: {e.Message}");
                Console.ReadLine();
                Console.WriteLine("\nAll details:");
                Console.WriteLine(e);
                Console.ReadLine();
                throw;
            }
        }
    }
}
