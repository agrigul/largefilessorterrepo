﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LargeFileSorter.Domain.MergeSort.InMemorySort;
using LargeFileSorter.Domain.MergeSort.MergeFiles;

namespace LargeFileSorter.Domain.MergeSort
{
    /// <summary>
    /// Sorts large files
    /// </summary>
    public class BottomUpMergeSortService
    {
        /// <summary>
        /// Fastest buffer size for this experiment
        /// </summary>
        protected const int BufferSizeForNtfs = 1024 * 2048;


        /// <summary>
        /// Reads data from file, sorts it and saves back
        /// </summary>
        /// <param name="pathToFile"></param>
        /// <param name="ramUsageSize"></param>
        /// <param name="maxNumberOfThreads"></param>
        /// <returns></returns>
        public async Task SortAsync(string pathToFile, long ramUsageSize, int? maxNumberOfThreads = null)
        {
            try
            {
                if (File.Exists(pathToFile) == false)
                {
                    throw new FileNotFoundException(nameof(pathToFile));
                }

                long numberOfFiles = CalculateNumberOfFilesToSplitOn(pathToFile, ramUsageSize, ref maxNumberOfThreads);


                var start = DateTime.Now;
                var listOfFiles = await SplitOnManyFilesAsync(pathToFile, numberOfFiles).ConfigureAwait(false);
                var splitTime = DateTime.Now - start;

                start = DateTime.Now;
                await SortingDataInChunkFiles(listOfFiles, maxNumberOfThreads.Value).ConfigureAwait(false);
                var sortTime = DateTime.Now - start;

                start = DateTime.Now;
                Console.WriteLine($"Start merging: {listOfFiles.Count} files");
                await MergeUsingLinkedListAndForManyFilesAsync(pathToFile, 0, listOfFiles.Count - 1, listOfFiles).ConfigureAwait(false);
                var mergeTime = DateTime.Now - start;

                Console.WriteLine($"\n\n#### Sorting of {listOfFiles.Count} files. Expected RAM usage {ramUsageSize} ###");
                Console.WriteLine($"Split: {splitTime.TotalSeconds} seconds using {maxNumberOfThreads} threads and {numberOfFiles} files.\n" +
                                  $"Sort all chunk files {sortTime.TotalSeconds} seconds\n" +
                                  $"Merge: {mergeTime.TotalSeconds} seconds\n");

                Console.WriteLine($"Total: {(splitTime + sortTime + mergeTime).TotalSeconds} seconds.");

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        /// <summary>
        /// Calculates number of files according to RAM limit and qty of threads to load: ramUsageSize >= SUM(maxNumberOfThreads * chunkFileSize)
        /// </summary>
        /// <param name="pathToFile"></param>
        /// <param name="ramUsageSize"></param>
        /// <param name="maxNumberOfThreads"></param>
        /// <returns></returns>
        protected long CalculateNumberOfFilesToSplitOn(string pathToFile, long ramUsageSize, ref int? maxNumberOfThreads)
        {
            long minNumberOfThreads = 1;
            if (maxNumberOfThreads < minNumberOfThreads)
            {
                throw new ArgumentOutOfRangeException(nameof(maxNumberOfThreads));
            }

            FileInfo info = new FileInfo(pathToFile);
            long numberOfPeacesAwaitableForRamWhenSort = info.Length / ramUsageSize;
            if (numberOfPeacesAwaitableForRamWhenSort == 0)
            {
                numberOfPeacesAwaitableForRamWhenSort = 1;
            }

            if (maxNumberOfThreads == null)
            {
                maxNumberOfThreads = Environment.ProcessorCount;
            }

            var numberOfFiles = numberOfPeacesAwaitableForRamWhenSort;

            return numberOfFiles;
        }


        /// <summary>
        /// Splits big files on files, which may be loaded to RAM
        /// </summary>
        /// <param name="pathToSourceFile">path to source file</param>
        /// <param name="numberOfFilesToRunSortingInMultipleThreads"></param>
        /// <returns></returns>
        protected async Task<IList<string>> SplitOnManyFilesAsync(string pathToSourceFile, long numberOfFilesToRunSortingInMultipleThreads)
        {
            Console.WriteLine("Start splitting...");
            FileInfo info = new FileInfo(pathToSourceFile);

            if (info.Length == 0)
            {
                return new List<string>();
            }

            string directoryName = Path.GetDirectoryName(pathToSourceFile);

            IList<string> pathToChunkList = new List<string>();
            if (numberOfFilesToRunSortingInMultipleThreads == 1)
            {
                string pathToChunkFile = Path.Combine(directoryName, "temp_1.txt");
                if (File.Exists(pathToChunkFile))
                {
                    File.Delete(pathToChunkFile);
                }
                File.Copy(pathToSourceFile, pathToChunkFile);
                pathToChunkList.Add(pathToChunkFile);
                return pathToChunkList;
            }

            StreamWriter[] writers = new StreamWriter[numberOfFilesToRunSortingInMultipleThreads];

            using (StreamReader sourceReader =
                new StreamReader(pathToSourceFile, Encoding.Default, true, BufferSizeForNtfs))
            {

                try
                {
                    while (sourceReader.EndOfStream == false)
                    {
                        IList<string> listOfNextLines = new List<string>();
                        for (int i = 0; i < writers.Length; i++)
                        {
                            var nextLine = await sourceReader.ReadLineAsync().ConfigureAwait(false);
                            if (nextLine != null)
                            {
                                listOfNextLines.Add(nextLine);
                            }
                        }

                        int writerIndex = 0;
                        IList<Task> tasks = new List<Task>();
                        for (int i = 0; i < listOfNextLines.Count; i++)
                        {
                            if (writers[writerIndex] == null)
                            {
                                string pathToChunkFile = Path.Combine(directoryName, $"temp_{writerIndex}.txt");
                                pathToChunkList.Add(pathToChunkFile);
                                if (File.Exists(pathToChunkFile))
                                {
                                    File.Delete(pathToChunkFile);
                                }
                                writers[writerIndex] = new StreamWriter(pathToChunkFile, true, Encoding.Default, BufferSizeForNtfs);
                            }

                            int currentWriterIndex = writerIndex;
                            int currentI = i;
                            tasks.Add(writers[currentWriterIndex].WriteLineAsync(listOfNextLines[currentI]));
                            writerIndex++;
                        }

                        await Task.WhenAll(tasks);

                    }

                }
                finally
                {
                    for (var i = 0; i < writers.Length; i++)
                    {
                        if (writers[i] != null)
                        {
                            await writers[i].DisposeAsync();
                            writers[i] = null;
                        }
                    }
                }

            }

            Console.WriteLine("Split is done.");

            return pathToChunkList;
        }


        /// <summary>
        /// Runs sorting in parallel, but uses limited number of threads not to exceed RAM limits
        /// </summary>
        /// <param name="pathToChunkList"></param>
        /// <param name="maxNumberOfThreads"></param>
        /// <returns></returns>
        protected async Task SortingDataInChunkFiles(IList<string> pathToChunkList, int maxNumberOfThreads)
        {
            Console.WriteLine("Sort chunks...");

            var fileProcessingQueue = new Queue<string>(pathToChunkList);

            var start = DateTime.Now;
            var allTasks = new List<Task>();
            int maxNumberOfHandlingFiles = maxNumberOfThreads / 2;
            if (maxNumberOfHandlingFiles == 0)
            {
                maxNumberOfHandlingFiles++;
            }
            while (fileProcessingQueue.Any())
            {
                if (allTasks.Count < maxNumberOfHandlingFiles)
                {
                    var path = fileProcessingQueue.Dequeue();
                    var notSortedList = ReadUnsortedChunkAsString(path);
                    var task = SortChunkAndSaveAsync(path, notSortedList, maxNumberOfThreads);
                    allTasks.Add(task);
                }
                else
                {
                    await Task.WhenAny(allTasks).ContinueWith((t) =>
                    {
                        if (t != null)
                        {
                            var removeTask = allTasks.FirstOrDefault(x => x.IsCompleted || x.IsFaulted || x.IsCanceled);
                            if (removeTask != null)
                            {
                                allTasks.Remove(removeTask);
                            }
                        }
                    });
                }
            }
            await Task.WhenAll(allTasks).ConfigureAwait(false);


            var end = DateTime.Now - start;
            Console.WriteLine($"All chunk files were sorted in {end.TotalSeconds}");
        }

        protected List<SortingRecord> ReadUnsortedChunk(string pathToChunkFile)
        {
            List<SortingRecord> notSortedList = new List<SortingRecord>();

            var fileName = Path.GetFileName(pathToChunkFile);
            long loadedSize = 0;
            int previousPercent = 0;
            FileInfo fileInfo = new FileInfo(pathToChunkFile);


            using (StreamReader reader = new StreamReader(pathToChunkFile, Encoding.Default, true, BufferSizeForNtfs))
            {
                while (reader.EndOfStream == false)
                {
                    var line = reader.ReadLine();

                    notSortedList.Add(new SortingRecord(line));

                    loadedSize += line?.Length ?? 0;
                    var percentsLoaded = (int)(loadedSize * 100 / fileInfo.Length);

                    if (percentsLoaded > previousPercent)
                    {
                        WriteToSameLine($"Loaded {percentsLoaded} % ({loadedSize / 1024 / 1024} MB)");
                        previousPercent = percentsLoaded;
                    }

                }
            }

            WriteToSameLine(string.Empty);
            Console.WriteLine($"File {fileName} was 100% loaded ({loadedSize / 1024 / 1024} MB)");

            return notSortedList;
        }


        protected List<string> ReadUnsortedChunkAsString(string pathToChunkFile)
        {
            List<string> notSortedList = new List<string>();

            var fileName = Path.GetFileName(pathToChunkFile);
            long loadedSize = 0;
            int previousPercent = 0;
            FileInfo fileInfo = new FileInfo(pathToChunkFile);


            using (StreamReader reader = new StreamReader(pathToChunkFile, Encoding.Default, true, BufferSizeForNtfs))
            {
                while (reader.EndOfStream == false)
                {
                    var line = reader.ReadLine();

                    notSortedList.Add(line);

                    loadedSize += line?.Length ?? 0;
                    var percentsLoaded = (int)(loadedSize * 100 / fileInfo.Length);

                    if (percentsLoaded > previousPercent)
                    {
                        WriteToSameLine($"Loaded {percentsLoaded} % ({loadedSize / 1024 / 1024} MB)");
                        previousPercent = percentsLoaded;
                    }

                }
            }

            WriteToSameLine(string.Empty);
            Console.WriteLine($"File {fileName} was 100% loaded ({loadedSize / 1024 / 1024} MB)");

            return notSortedList;
        }

        /// <summary>
        /// Sorts chunk in RAM with built in quick sort and then saves to chunk file
        /// </summary>
        /// <param name="pathToChunkFile"></param>
        /// <param name="notSortedListOfStrings"></param>
        /// <param name="maxNumberOfThreads"></param>
        /// <returns></returns>
        protected async Task SortChunkAndSaveAsync(string pathToChunkFile, List<string> notSortedListOfStrings, int maxNumberOfThreads)
        {
            if (pathToChunkFile == null) throw new ArgumentNullException(nameof(pathToChunkFile));
            if (notSortedListOfStrings == null) throw new ArgumentNullException(nameof(notSortedListOfStrings));
            if (maxNumberOfThreads < 1) throw new ArgumentOutOfRangeException(nameof(maxNumberOfThreads));

            var startChunk = DateTime.Now;

            var fileName = Path.GetFileName(pathToChunkFile);
            try
            {
                File.Delete(pathToChunkFile);

                if (notSortedListOfStrings.Any())
                {

                    var comparer = new SortingRecordComparer();
                    var mergeSortAlgorithm = new MergeSortAlgorithm();


                    List<SortingRecord> sorted;
                    const int mergeConstant = 32;
                    if (notSortedListOfStrings.Count > mergeConstant)
                    {
                        int take = notSortedListOfStrings.Count / mergeConstant;
                        if (take < mergeConstant)
                        {
                            take = mergeConstant;
                        }
                        int skip = 0;

                        ConcurrentBag<Queue<SortingRecord>> listOfQueues = new ConcurrentBag<Queue<SortingRecord>>();


                        var allTasks = new List<Task>();

                        int localMaxNumberOfThreads = maxNumberOfThreads / 2;
                        if (localMaxNumberOfThreads == 0 ||
                            localMaxNumberOfThreads % 3 == 0)
                        {
                            localMaxNumberOfThreads++;
                        }
                        

                        while (skip < notSortedListOfStrings.Count)
                        {

                            if (allTasks.Count < localMaxNumberOfThreads)
                            {

                                var unsortedPortion = notSortedListOfStrings.Skip(skip).Take(take).ToList();
                                skip += take;

                                Console.WriteLine($"Tasks count: {allTasks.Count}, skipped {skip}, portion #{listOfQueues.Count}, file: {fileName}");

                                var task = Task.Run(() =>
                                {
                                    var sortedPortion = unsortedPortion.Select(x => new SortingRecord(x)).ToList();
                                    sortedPortion.Sort(comparer);
                                    listOfQueues.Add(new Queue<SortingRecord>(sortedPortion));
                                });

                                allTasks.Add(task);
                            }
                            else
                            {
                                await Task.WhenAny(allTasks.ToArray()).ContinueWith((t) =>
                                {
                                    if (t != null)
                                    {
                                        var removeTask = allTasks.FirstOrDefault(x => x.IsCompleted || x.IsFaulted || x.IsCanceled);
                                        if (removeTask != null)
                                        {
                                            allTasks.Remove(removeTask);
                                        }
                                    }
                                });
                            }
                        }

                        await Task.WhenAll(allTasks);

                        sorted = mergeSortAlgorithm.MergeWithLinkedList(listOfQueues.ToList());

                    }
                    else
                    {
                        sorted = notSortedListOfStrings.Select(x => new SortingRecord(x)).ToList();
                        sorted.Sort(comparer);
                    }

                    
                    using (StreamWriter writer =
                        new StreamWriter(pathToChunkFile, true, Encoding.Default, BufferSizeForNtfs))
                    {
                        foreach (var sortedLine in sorted)
                        {
                            writer.WriteLine(sortedLine.OriginalValue);
                        }
                    }
                }


                var endOfChunk = DateTime.Now - startChunk;
                Console.WriteLine($"{Path.GetFileName(pathToChunkFile)} was sorted int {endOfChunk.TotalSeconds}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void WriteToSameLine(string value)
        {
            try
            {
                var cursorTop = Console.CursorTop - 1;
                if (cursorTop < 0)
                {
                    cursorTop = 0;
                }
                Console.SetCursorPosition(0, cursorTop);
            }
            catch
            {
                // suppress
            }

            Console.WriteLine(value);
        }

        /// <summary>
        /// Merges several files. If there is more then 32 files, it should reduce its number until 32 merging recursively
        /// </summary>
        /// <param name="pathToDestFile"></param>
        /// <param name="leftIndex"></param>
        /// <param name="rightIndex"></param>
        /// <param name="listOfFilesWithChunks"></param>
        /// <returns></returns>
        protected async Task MergeUsingLinkedListAndForManyFilesAsync(string pathToDestFile, int leftIndex, int rightIndex,
            IList<string> listOfFilesWithChunks)
        {
            const int optimalSizeForMerge = 32;
            WriteToSameLine($"Merging into {Path.GetFileName(pathToDestFile)}.");
            try
            {
                string directoryName = Path.GetDirectoryName(pathToDestFile);

                IList<string> listOfFilesToSort;
                if (rightIndex - leftIndex > optimalSizeForMerge)
                {
                    int middle = leftIndex + (rightIndex - leftIndex) / 2;

                    string leftFileName = Path.Combine(directoryName, $"temp_merge_L{leftIndex}R{middle}.txt");
                    string rightFileName = Path.Combine(directoryName, $"temp_merge_L{middle + 1}R{rightIndex}.txt");

                    await MergeUsingLinkedListAndForManyFilesAsync(leftFileName, leftIndex, middle, listOfFilesWithChunks);
                    await MergeUsingLinkedListAndForManyFilesAsync(rightFileName, middle + 1, rightIndex,
                        listOfFilesWithChunks);


                    listOfFilesToSort = new List<string> { leftFileName, rightFileName };
                }
                else
                {
                    int take = (rightIndex - leftIndex) + 1;
                    listOfFilesToSort = listOfFilesWithChunks.Skip(leftIndex).Take(take).ToList();
                }


                await MergeAndSaveInParallelUsingLinkedList(pathToDestFile, listOfFilesToSort);

                Console.WriteLine("All files were merged");

                foreach (string fileToDelete in listOfFilesToSort)
                {
                    if (File.Exists(fileToDelete))
                    {
                        File.Delete(fileToDelete);
                    }
                }

                Console.WriteLine("All temp files were deleted");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }


        /// <summary>
        /// Merges 32 instead of 2 queues into single one
        /// </summary>
        /// <param name="pathToDestFile"></param>
        /// <param name="listOfFilesWithChunks"></param>
        /// <returns></returns>
        private async Task MergeAndSaveInParallelUsingLinkedList(string pathToDestFile, IList<string> listOfFilesWithChunks)
        {

            if (listOfFilesWithChunks == null)
            {
                throw new ArgumentNullException(nameof(listOfFilesWithChunks));
            }

            listOfFilesWithChunks = listOfFilesWithChunks.Where(x => string.IsNullOrWhiteSpace(x) == false
                                                                     && File.Exists(x)).ToList();

            if (listOfFilesWithChunks.Any() == false)
            {
                throw new ArgumentOutOfRangeException(nameof(listOfFilesWithChunks), "there is no valid paths to chunks");
            }


            if (File.Exists(pathToDestFile))
            {
                File.Delete(pathToDestFile);
            }

            StreamReader[] arrayOfChunkReaders = new StreamReader[listOfFilesWithChunks.Count];
            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(pathToDestFile, true, Encoding.Default, BufferSizeForNtfs);

                List<StreamAndValue> notSortedList = new List<StreamAndValue>();
                LinkedList<StreamAndValue> fileIndexAndNextValueInSortedList = new LinkedList<StreamAndValue>();

                for (int i = 0; i < listOfFilesWithChunks.Count; i++)// first init to set first values
                {
                    if (arrayOfChunkReaders[i] == null)
                    {
                        if (listOfFilesWithChunks[i] == null)
                        {
                            throw new ArgumentNullException(nameof(arrayOfChunkReaders), "File path was not found");
                        }

                        arrayOfChunkReaders[i] = new StreamReader(listOfFilesWithChunks[i], Encoding.Default, true,
                            BufferSizeForNtfs);

                        string nextLine = await arrayOfChunkReaders[i].ReadLineAsync().ConfigureAwait(false);


                        notSortedList.Add(new StreamAndValue(i, nextLine));
                    }
                }

                var customComparer = new TextThenNumberComparer();
                foreach (StreamAndValue item in notSortedList.OrderBy(x => x.TextValue, customComparer))
                {
                    fileIndexAndNextValueInSortedList.AddLast(new StreamAndValue(item.StreamId, item.TextValue));
                }

                while (arrayOfChunkReaders.Any(x => x.EndOfStream == false) || fileIndexAndNextValueInSortedList.Any())
                {

                    var nextMinValue = fileIndexAndNextValueInSortedList.First?.Value;

                    if (nextMinValue != null)
                    {
                        await writer.WriteLineAsync(nextMinValue.TextValue);
                        fileIndexAndNextValueInSortedList.RemoveFirst();

                        if (arrayOfChunkReaders[nextMinValue.StreamId].EndOfStream == false)
                        {
                            var nextLine = await arrayOfChunkReaders[nextMinValue.StreamId].ReadLineAsync();

                            while (nextLine == nextMinValue.TextValue && arrayOfChunkReaders[nextMinValue.StreamId].EndOfStream == false)
                            {
                                // optimization in case of sequence of equal values
                                await writer.WriteLineAsync(nextMinValue.TextValue);
                                nextLine = await arrayOfChunkReaders[nextMinValue.StreamId].ReadLineAsync();
                            }

                            bool wasPositionFound = false;
                            var first = fileIndexAndNextValueInSortedList.First;


                            LinkedListNode<StreamAndValue> currentNextLinePosition = first;
                            int counter = 0;
                            while (counter < fileIndexAndNextValueInSortedList.Count)
                            {
                                counter++;
                                if (currentNextLinePosition == null)
                                {
                                    wasPositionFound = true;
                                    break;
                                }

                                if (customComparer.Compare(nextLine, currentNextLinePosition.Value.TextValue) <= 0)
                                {
                                    wasPositionFound = true;
                                    break;
                                }

                                currentNextLinePosition = currentNextLinePosition.Next;
                            }

                            var nextValueToAdd = new StreamAndValue(nextMinValue.StreamId, nextLine);
                            if (wasPositionFound)
                            {

                                if (currentNextLinePosition != null)
                                {
                                    fileIndexAndNextValueInSortedList.AddBefore(currentNextLinePosition, nextValueToAdd);
                                }
                                else
                                {
                                    fileIndexAndNextValueInSortedList.AddFirst(nextValueToAdd);
                                }
                            }
                            else
                            {
                                fileIndexAndNextValueInSortedList.AddLast(nextValueToAdd);
                            }

                        }
                    }
                }
            }
            finally
            {
                foreach (var chunkReader in arrayOfChunkReaders)
                {
                    chunkReader?.Dispose();
                }

                writer?.DisposeAsync();
            }

            foreach (var pathToChunk in listOfFilesWithChunks)
            {
                if (File.Exists(pathToChunk))
                {
                    File.Delete(pathToChunk);
                }
            }
        }
    }
}
