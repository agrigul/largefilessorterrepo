﻿namespace LargeFileSorter.Domain.MergeSort.MergeFiles
{
    public class StreamAndValue
    {
        public int StreamId { get; }
        public string TextValue { get; }

        public StreamAndValue(int streamId, string textValue)
        {
            StreamId = streamId;
            TextValue = textValue;
        }
    }
}