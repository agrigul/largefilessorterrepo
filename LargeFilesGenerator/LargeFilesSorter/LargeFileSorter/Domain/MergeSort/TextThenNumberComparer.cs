﻿using System;
using System.Collections.Generic;

namespace LargeFileSorter.Domain.MergeSort
{
    /// <summary>
    /// Custom comparer for algorithm
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TextThenNumberComparer : IComparer<string>
    {
        /// <summary>
        /// Compares record. First compares text part, then number
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public int Compare(string x, string y)
        {
            if (x == y)
            {
                return 0;
            }
            
            if (x == null || y == null)
            {
                throw new FormatException("null values");
            }
            
            (long numberX, string textX) = Split(x);
            (long numberY, string textY) = Split(y);

            int compareTextResult = string.Compare(textX, textY, StringComparison.Ordinal);

            if (compareTextResult == 0)
            {

                if (numberX > numberY)
                {
                    return 1;
                }
                else
                {
                    if (numberX < numberY)
                    {
                        return -1;
                    }

                    return 0;
                }
            }
            else
            {
                if (compareTextResult < 0) return -1;
                else return 1;
            }
        }

        /// <summary>
        /// Splits string on number and string part. validates values
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private (long, string) Split(string value)
        {
            var dotIndex = value.IndexOf('.');
            if (dotIndex < 0)
            {
                throw new FormatException("Invalid structure");
            }

            var numberAsString = value.Substring(0, dotIndex);
            var textPart = value.Substring(dotIndex + 1, value.Length - dotIndex - 1);

            if (long.TryParse(numberAsString, out long numberPart) == false)
            {
                throw new FormatException($"Invalid number: {value}");
            }

            if (string.IsNullOrWhiteSpace(textPart))
            {
                throw new FormatException($"There is no text part in this record: {value}");
            }

            return (numberPart, textPart);

        }


    }
}