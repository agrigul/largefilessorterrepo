﻿
namespace LargeFileSorter.Domain.MergeSort.InMemorySort
{
    /// <summary>
    /// A representation of the line for sorting
    /// </summary>
    public class SortingRecord
    {
        public long Number { get; }
        public string Text { get; }
        public string OriginalValue => Number + ". " + Text;

        public SortingRecord(string value)
        {
            var dotIndex = value.IndexOf('.');
            var numberAsString = value.Substring(0, dotIndex);
            Number = long.Parse(numberAsString);
            Text = value.Substring(dotIndex + 1, value.Length - dotIndex - 1).TrimStart();
         
        }

    }
}