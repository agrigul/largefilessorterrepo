﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LargeFileSorter.Domain.MergeSort.InMemorySort
{
    public class MergeSortAlgorithm
    {
        public static SortingRecordComparer CustomComparer = new SortingRecordComparer();


        public List<SortingRecord> MergeSort(List<string> unsortedList)
        {
            if (unsortedList == null)
            {
                return null;
            }

            return MergeSort(unsortedList.Select(x => new SortingRecord(x)).ToList());
        }


        /// <summary>
        /// Sorts records with merge sort
        /// </summary>
        /// <param name="unsortedList"></param>
        /// <returns></returns>
        public List<SortingRecord> MergeSort(List<SortingRecord> unsortedList)
        {
            if (unsortedList == null)
            {
                throw new ArgumentNullException(nameof(unsortedList));
            }

            if (unsortedList.Count <= 1)
            {
                return unsortedList;
            }


            int middle = unsortedList.Count / 2;

            List<SortingRecord> leftList = unsortedList.Skip(0).Take(middle).ToList();
            leftList = MergeSort(leftList);

            List<SortingRecord> rightList = unsortedList.Skip(middle).Take(unsortedList.Count - middle).ToList();
            rightList = MergeSort(rightList);


            var res = Merge(leftList, rightList);
            return res;

        }


        /// <summary>
        /// Sorts records  with merge sort. Async version
        /// </summary>
        /// <param name="unsortedList"></param>
        /// <returns></returns>
        public async Task<List<SortingRecord>> MergeSortAsync(List<SortingRecord> unsortedList)
        {
            if (unsortedList == null)
            {
                throw new ArgumentNullException(nameof(unsortedList));
            }

            if (unsortedList.Count <= 1)
            {
                return unsortedList;
            }

            int middle = unsortedList.Count / 2;
            List<SortingRecord> leftList = unsortedList.Skip(0).Take(middle).ToList();
            var leftTask = Task.Run(() => MergeSortAsync(leftList));


            List<SortingRecord> rightList = unsortedList.Skip(middle).Take(unsortedList.Count - middle).ToList();
            var rightTask = Task.Run(() => MergeSortAsync(rightList));

            await Task.WhenAll(leftTask, rightTask);

            var res = Merge(leftTask.Result, rightTask.Result);
            return res;

        }
        
        /// <summary>
        /// Uses merge of files with linked list to find min value
        /// </summary>
        /// <param name="arrayOfPreSortedQueues"></param>
        /// <returns></returns>
        public List<SortingRecord> MergeWithLinkedList(List<Queue<SortingRecord>> arrayOfPreSortedQueues)
        {
            List<SortingRecord> result = new List<SortingRecord>();

            var sortedFirstValuesFromQueues = new List<SortingRecord>();
            foreach (var queue in arrayOfPreSortedQueues)
            {
                sortedFirstValuesFromQueues.Add(queue.Dequeue());
            }

            sortedFirstValuesFromQueues = MergeSort(sortedFirstValuesFromQueues);

            LinkedList<IndexAndRecord> sortedLinkedList = new LinkedList<IndexAndRecord>();
            for (var i = 0; i < sortedFirstValuesFromQueues.Count; i++)
            {
                SortingRecord sortingRecord = sortedFirstValuesFromQueues[i];
                sortedLinkedList.AddLast(new IndexAndRecord(i, sortingRecord));
            }


            while (arrayOfPreSortedQueues.Any(x => x.Any())) // has values in queues
            {
                var recordWithMinValue = sortedLinkedList.First;
                if (recordWithMinValue == null)
                {
                    Console.WriteLine("sortedLinkedList.First is null in " + nameof(MergeWithLinkedList));
                    break;
                }

                int indexOfQueueWithMinValue = recordWithMinValue.Value.Id;
                result.Add(recordWithMinValue.Value.SortingValue);
                sortedLinkedList.RemoveFirst();

                SortingRecord nextValueFromQueue;
                if (arrayOfPreSortedQueues[indexOfQueueWithMinValue].Any())
                {
                    nextValueFromQueue = arrayOfPreSortedQueues[indexOfQueueWithMinValue].Dequeue();
                }
                else
                {
                    continue;
                }

                while (nextValueFromQueue == recordWithMinValue.Value.SortingValue && arrayOfPreSortedQueues[indexOfQueueWithMinValue].Any())
                {
                    // optimization in case of sequence of equal values
                    result.Add(nextValueFromQueue);
                    nextValueFromQueue = arrayOfPreSortedQueues[indexOfQueueWithMinValue].Dequeue();
                }


                var currentFirstRecord = sortedLinkedList.First;
                int counter = 0;
                bool wasPositionFound = false;

                while (wasPositionFound == false && counter < sortedLinkedList.Count)
                {
                    counter++;
                    if (currentFirstRecord == null)
                    {
                        wasPositionFound = true;
                        break;
                    }

                    if (CustomComparer.Compare(nextValueFromQueue, currentFirstRecord.Value.SortingValue) <= 0)
                    {
                        wasPositionFound = true;
                        break;
                    }

                    currentFirstRecord = currentFirstRecord.Next;
                }

                IndexAndRecord nextValueToAdd = new IndexAndRecord(indexOfQueueWithMinValue, nextValueFromQueue);
                if (wasPositionFound)
                {

                    if (currentFirstRecord != null)
                    {
                        sortedLinkedList.AddBefore(currentFirstRecord, nextValueToAdd);
                    }
                    else
                    {
                        sortedLinkedList.AddFirst(nextValueToAdd);
                    }
                }
                else
                {
                    sortedLinkedList.AddLast(nextValueToAdd);
                }

            }

            var nextValue = sortedLinkedList.First;
            if (nextValue != null)
            {
                int tailCounter = 0;
                while (tailCounter < sortedLinkedList.Count)
                {
                    result.Add(nextValue.Value.SortingValue);
                    nextValue = nextValue.Next;
                    tailCounter++;
                }
            }

            return result;

        }
        

        
        private List<SortingRecord> Merge(List<SortingRecord> leftList, List<SortingRecord> rightList)
        {
            int l = 0;
            int r = 0;

            List<SortingRecord> result = new List<SortingRecord>(leftList.Count + rightList.Count);
            while (l < leftList.Count && r < rightList.Count)
            {

                if (CustomComparer.Compare(leftList[l], rightList[r]) < 0)
                {
                    result.Add(leftList[l]);
                    l++;
                }
                else
                {
                    if (CustomComparer.Compare(leftList[l], rightList[r]) > 0)
                    {
                        result.Add(rightList[r]);
                        r++;
                    }
                    else
                    {
                        result.Add(leftList[l]);
                        l++;
                        result.Add(rightList[r]);
                        r++;
                    }
                }
            }

            while (l < leftList.Count)
            {
                result.Add(leftList[l]);
                l++;
            }

            while (r < rightList.Count)
            {
                result.Add(rightList[r]);
                r++;
            }

            return result;

        }

    }
    
}
