﻿using System;
using System.Collections.Generic;

namespace LargeFileSorter.Domain.MergeSort.InMemorySort
{
    /// <summary>
    /// Custom comparer for algorithm
    /// </summary>
    public class SortingRecordComparer : IComparer<SortingRecord>
    {
        /// <summary>
        /// Compares record. First compares text part, then number
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public int Compare(SortingRecord x, SortingRecord y)
        {
            if (x == y)
            {
                return 0;
            }

            if (x == null)
            {
                return -1;
            }

            if (y == null)
            {
                return 1;
            }

            int compareTextResult = string.Compare(x.Text, y.Text, StringComparison.Ordinal);

            if (compareTextResult == 0)
            {

                if (x.Number > y.Number)
                {
                    return 1;
                }
                else
                {
                    if (x.Number < y.Number)
                    {
                        return -1;
                    }

                    return 0;
                }
            }

            if (compareTextResult < 0)
            {
                return -1;
            }
            return 1;

        }

    }
}