﻿namespace LargeFileSorter.Domain.MergeSort.InMemorySort
{
    /// <summary>
    /// Stores index id of the used Queue and record value
    /// </summary>
    public class IndexAndRecord
    {
        public int Id { get; }
        public SortingRecord SortingValue { get; }

        public IndexAndRecord(int id, SortingRecord sortingValue)
        {
            Id = id;
            SortingValue = sortingValue;
        }
    }
}