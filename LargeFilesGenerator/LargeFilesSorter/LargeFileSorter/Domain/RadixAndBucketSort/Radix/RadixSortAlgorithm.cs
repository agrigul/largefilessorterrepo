﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LargeFileSorter.Domain.RadixAndBucketSort.Radix
{
    public class RadixSortAlgorithm
    {
        const int EncodingMinLetter = ' ';
        const int EncodingMaxLetter = '~';


        public List<string> Sort(List<string> listOfLinesToSort)
        {
            List<RadixSortingRecord> listOfRecordsToSort = new List<RadixSortingRecord>();

            int maxLength = 0;
            foreach (var nextLine in listOfLinesToSort)
            {
                if (nextLine.Length >= maxLength)
                {
                    maxLength = nextLine.Length;
                }
                listOfRecordsToSort.Add(new RadixSortingRecord(nextLine));
            }

            var result = SortMsd(listOfRecordsToSort);
            return result.Select(x => x.OriginalValue).ToList();

        }



        public List<RadixSortingRecord> SortMsd(List<RadixSortingRecord> listToSort, int? maxLineLength = null)
        {
            maxLineLength ??= listToSort.Max(x => x.Text.Length);

            if (maxLineLength < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(maxLineLength));
            }

            var result = SortMsd(listToSort, nameof(RadixSortingRecord.Text), 0, listToSort.Count, 0, maxLineLength.Value - 1);
            return result;

        }


        public List<string> SortMsd(List<string> listToSort)
        {
            int maxLineLength = listToSort.Max(x => x.Length);

            var result = BackSortMsd(listToSort, 0, listToSort.Count, maxLineLength - 1);
            return result;

        }
        private List<string> SortMsd(List<string> listToSort, int left, int right, int nextLetterIndex, int maxLineLength)
        {
            if (left >= right || nextLetterIndex >= maxLineLength)
            {
                return listToSort;
            }

            Dictionary<char, List<string>> sortedByLetterBuckets = new Dictionary<char, List<string>>();
            for (int i = EncodingMinLetter; i < EncodingMaxLetter; i++)
            {
                sortedByLetterBuckets.Add((char)i, new List<string>());
            }


            for (int i = left; i < right; i++)
            {
                var line = listToSort[i];


                char letter;
                if (line.Length <= nextLetterIndex)
                {
                    letter = ' '; // found end of string for current line(value)
                }
                else
                {
                    letter = line[nextLetterIndex];
                }

                var partiallySortedStrings = sortedByLetterBuckets[letter];
                partiallySortedStrings.Add(line);
            }


            sortedByLetterBuckets = WriterSortedElementsToMainList(listToSort, left, right, sortedByLetterBuckets);

            int l = left;
            foreach (var bucket in sortedByLetterBuckets)
            {
                if (bucket.Value.Count > 1)
                {
                    listToSort = SortMsd(listToSort, l, l + bucket.Value.Count, nextLetterIndex + 1, maxLineLength);
                }

                l = l + bucket.Value.Count;
            }


            return listToSort;
        }


        private List<string> BackSortMsd(List<string> listToSort, int left, int right, int nextLetterIndex)
        {
            if (left >= right || nextLetterIndex == 0)
            {
                return listToSort;
            }

            Dictionary<char, List<string>> sortedByLetterBuckets = new Dictionary<char, List<string>>();
            for (int i = EncodingMinLetter; i < EncodingMaxLetter; i++)
            {
                sortedByLetterBuckets.Add((char)i, new List<string>());
            }


            for (int i = left; i < right; i++)
            {
                var line = listToSort[i];


                char letter;
                if (line.Length < nextLetterIndex)
                {
                    letter = ' '; // found end of string for current line(value)
                }
                else
                {
                    letter = line[nextLetterIndex];
                }

                var partiallySortedStrings = sortedByLetterBuckets[letter];
                partiallySortedStrings.Add(line);
            }


            sortedByLetterBuckets = WriterSortedElementsToMainList(listToSort, left, right, sortedByLetterBuckets);

            int l = left;
            foreach (var bucket in sortedByLetterBuckets)
            {
                if (bucket.Value.Count > 1)
                {
                    listToSort = BackSortMsd(listToSort, l, l + bucket.Value.Count, nextLetterIndex - 1);
                }

                l = l + bucket.Value.Count;
            }


            return listToSort;
        }


        private List<RadixSortingRecord> SortMsd(List<RadixSortingRecord> listToSort, string nameOfColumn, int leftBucketIndex, int rightBucketIndex, int nextLetterIndex, int maxLineLength)
        {
            if (leftBucketIndex >= rightBucketIndex || nextLetterIndex >= maxLineLength)
            {
                // then by Number
                if (nameOfColumn == nameof(RadixSortingRecord.Text))
                {
                    int maxLengthOfNumberPart = listToSort.Max(x => x.Number.Length);
                    listToSort = SortMsd(listToSort, nameof(RadixSortingRecord.Number), leftBucketIndex, rightBucketIndex, 0, maxLengthOfNumberPart);
                }
                return listToSort;
            }

            Dictionary<char, List<RadixSortingRecord>> sortedByLetterBuckets = new Dictionary<char, List<RadixSortingRecord>>();
            for (int i = EncodingMinLetter; i < EncodingMaxLetter; i++)
            {
                sortedByLetterBuckets.Add((char)i, new List<RadixSortingRecord>());
            }


            for (int i = leftBucketIndex; i < rightBucketIndex; i++)
            {
                var nextRecord = listToSort[i];
                string line = null;
                if (nameOfColumn == nameof(RadixSortingRecord.Text))
                {
                    line = nextRecord.Text;
                }

                if (nameOfColumn == nameof(RadixSortingRecord.Number))
                {
                    int lengthDiff = maxLineLength - nextRecord.Number.Length;
                    if (lengthDiff > 0)
                    {
                        var builder = new StringBuilder(maxLineLength);
                        builder.Append('0', lengthDiff);
                        builder.Append(nextRecord.Number);
                        line = builder.ToString();
                    }
                    else
                    {
                        line = nextRecord.Number;
                    }

                }

                if (line == null)
                {
                    return listToSort;
                }

                char letter;
                if (line.Length <= nextLetterIndex)
                {
                    letter = ' '; // found end of string for current line(value)
                }
                else
                {
                    letter = line[nextLetterIndex];
                }

                List<RadixSortingRecord> partiallySortedStrings = sortedByLetterBuckets[letter];
                partiallySortedStrings.Add(nextRecord);
            }


            sortedByLetterBuckets = WriterSortedElementsToMainList(listToSort, leftBucketIndex, rightBucketIndex, sortedByLetterBuckets);

            int l = leftBucketIndex;
            foreach (var bucket in sortedByLetterBuckets)
            {
                if (bucket.Value.Count > 1)
                {
                    listToSort = SortMsd(listToSort, nameOfColumn, l, l + bucket.Value.Count, nextLetterIndex + 1, maxLineLength);
                }

                l = l + bucket.Value.Count;
            }


            return listToSort;
        }


        /// <summary>
        /// Writes sorted bucket to main list by index
        /// </summary>
        /// <param name="listToSort"></param>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <param name="sortedByLetterBuckets"></param>
        /// <returns></returns>
        private Dictionary<char, List<string>> WriterSortedElementsToMainList(List<string> listToSort, int left, int right,
            Dictionary<char, List<string>> sortedByLetterBuckets)
        {
            sortedByLetterBuckets =
                sortedByLetterBuckets.Where(x => x.Value.Any())
                    .ToDictionary(x => x.Key, x => x.Value);


            if (sortedByLetterBuckets.Count < 1)
            {
                return sortedByLetterBuckets;
            }

            var sortedBucket = new List<string>();
            foreach (var bucket in sortedByLetterBuckets)
            {
                sortedBucket.AddRange(bucket.Value);
            }

            for (int i = left, j = 0; i < right; i++, j++)
            {
                listToSort[i] = sortedBucket[j];
            }

            return sortedByLetterBuckets;
        }



        private Dictionary<char, List<RadixSortingRecord>> WriterSortedElementsToMainList(List<RadixSortingRecord> listToSort, int left, int right,
            Dictionary<char, List<RadixSortingRecord>> sortedByLetterBuckets)
        {
            sortedByLetterBuckets =
                sortedByLetterBuckets.Where(x => x.Value.Any())
                    .ToDictionary(x => x.Key, x => x.Value);


            if (sortedByLetterBuckets.Count < 1)
            {
                return sortedByLetterBuckets;
            }

            var sortedBucket = new List<RadixSortingRecord>();
            foreach (var bucket in sortedByLetterBuckets)
            {
                sortedBucket.AddRange(bucket.Value);
            }

            for (int i = left, j = 0; i < right; i++, j++)
            {
                listToSort[i] = sortedBucket[j];
            }

            return sortedByLetterBuckets;
        }

    }
}
