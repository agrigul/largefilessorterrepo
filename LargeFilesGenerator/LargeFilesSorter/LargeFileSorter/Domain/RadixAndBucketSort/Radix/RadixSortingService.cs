﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LargeFileSorter.Domain.MergeSort.InMemorySort;
using LargeFileSorter.Domain.RadixAndBucketSort.Radix.Splitting;

namespace LargeFileSorter.Domain.RadixAndBucketSort.Radix
{
    public class RadixSortingService
    {
        public const int RecommendedBufferSize = 1024 * 1024;
        private readonly RadixSortAlgorithm sortingAlgorithm = new RadixSortAlgorithm();


        public async Task<string> SortAsync(long maxFileSize, string pathToSourceFile, string pathToDestFile = null)
        {

            SplitByAlphabet splitService = new SplitByAlphabet();

            var start = DateTime.Now;
            var splitFiles = await splitService.SplitFiles(pathToSourceFile, maxFileSize, 0);
            var splitTime = DateTime.Now - start;

            Console.WriteLine($"Split time {splitTime.TotalSeconds}");

            //IList<Task> allTasks = new List<Task>();
            //
            // start = DateTime.Now;
            //foreach (var splitFile in splitFiles)
            //{
            //    var task = Task.Run(() =>
            //    {
            //        return SortSingleFile(splitFile);
            //    });

            //    allTasks.Add(task);
            //}
            //await Task.WhenAll(allTasks);



            start = DateTime.Now;
            //int numberOfThreads = Environment.ProcessorCount;
            int numberOfThreads = 16;
            await SortingDataInChunks(splitFiles, numberOfThreads);




            var sortTime = DateTime.Now - start;
            Console.WriteLine($"Sort time {sortTime.TotalSeconds} of {splitFiles.Count}, av. size {maxFileSize}");

            start = DateTime.Now;
            await splitService.MergeFiles(splitFiles, pathToSourceFile);
            var mergeTime = DateTime.Now - start;

            Console.WriteLine($"Merge time {mergeTime.TotalSeconds}");

            long fileSizeMb = new FileInfo(pathToSourceFile).Length / 1024 / 1024;
            Console.WriteLine($"**** Sorting of {fileSizeMb} MB file is done. Total time: {(splitTime + sortTime + mergeTime).TotalSeconds} ****");
            return pathToDestFile;
        }


        private async Task SortingDataInChunks(IList<string> pathToChunkList, long maxNumberOfThreads)
        {
            IList<Task> allTasks = new List<Task>();

            long currentNumberOfProcessingFiles = 0;

            var fileProcessingQueue = new Queue<string>(pathToChunkList);

            if (pathToChunkList.Count <= maxNumberOfThreads)
            {
                maxNumberOfThreads = pathToChunkList.Count;
            }
            while (fileProcessingQueue.Any())
            {
                if (currentNumberOfProcessingFiles < maxNumberOfThreads)
                {
                    var path = fileProcessingQueue.Dequeue();
                    var task = Task.Run(() => SortSingleFileWithQuickSort(path));
                    allTasks.Add(task);
                    currentNumberOfProcessingFiles++;
                }
                else
                {
                    await Task.WhenAny(allTasks.ToArray()).ContinueWith((t) =>
                    {
                        if (t != null && (t.IsCompleted || t.IsCanceled))
                        {
                            var removeTask = allTasks.FirstOrDefault(x => x.IsCompleted);
                            if (removeTask != null)
                            {
                                allTasks.Remove(removeTask);
                            }
                            currentNumberOfProcessingFiles--;
                        }
                    });
                }
            }

            await Task.WhenAll(allTasks.ToArray());

        }


        public async Task<string> SortSingleFileWithMergeSort(string pathToSourceFile, string pathToDestFile = null)
        {
            pathToDestFile ??= pathToSourceFile;

            if (File.Exists(pathToSourceFile) == false)
            {
                throw new FileNotFoundException(pathToSourceFile);
            }

            if (File.Exists(pathToDestFile) && pathToDestFile != pathToSourceFile)
            {
                File.Delete(pathToDestFile);
            }


            var notSortedList = new List<SortingRecord>();
            using (StreamReader reader = new StreamReader(pathToSourceFile, Encoding.Default, true, RecommendedBufferSize))
            {
                while (reader.EndOfStream == false)
                {
                    var nextLine = await reader.ReadLineAsync();
                    var nextRecord = new SortingRecord(nextLine);
                    notSortedList.Add(nextRecord);
                }
            }



            if (notSortedList.Any())
            {
                var mergeSortAlgorithm = new MergeSortAlgorithm();

                List<SortingRecord> sortedRecords;

                const int mergeConstant = 32;
                if (notSortedList.Count > mergeConstant * 2)
                {
                    int take = notSortedList.Count / mergeConstant;
                    int skip = 0;
                    List<Queue<SortingRecord>> listOfQueues = new List<Queue<SortingRecord>>();
                    List<SortingRecord> portion = notSortedList.Skip(0).Take(take).ToList();

                    while (portion.Any())
                    {
                        portion = mergeSortAlgorithm.MergeSort(portion);
                        listOfQueues.Add(new Queue<SortingRecord>(portion));
                        skip += take;
                        portion = notSortedList.Skip(skip).Take(take).ToList();
                    }

                    sortedRecords = mergeSortAlgorithm.MergeWithLinkedList(listOfQueues);

                }
                else
                {
                    sortedRecords = mergeSortAlgorithm.MergeSort(notSortedList.ToList());
                }


                await using (StreamWriter writer =
                    new StreamWriter(pathToDestFile, false, Encoding.Default, RecommendedBufferSize))
                {
                    foreach (SortingRecord sortedRecord in sortedRecords)
                    {
                        await writer.WriteLineAsync(sortedRecord.OriginalValue);
                    }
                }
            }

            return pathToDestFile;

        }
        public async Task<string> SortSingleFileWithRadixSort(string pathToSourceFile, string pathToDestFile = null)
        {
            pathToDestFile ??= pathToSourceFile;

            if (File.Exists(pathToSourceFile) == false)
            {
                throw new FileNotFoundException(pathToSourceFile);
            }

            if (File.Exists(pathToDestFile) && pathToDestFile != pathToSourceFile)
            {
                File.Delete(pathToDestFile);
            }


            var listToSort = new List<RadixSortingRecord>();
            using (StreamReader reader = new StreamReader(pathToSourceFile, Encoding.Default, true, RecommendedBufferSize))
            {
                while (reader.EndOfStream == false)
                {
                    var nextLine = await reader.ReadLineAsync();
                    var nextRecord = new RadixSortingRecord(nextLine);
                    listToSort.Add(nextRecord);
                }
            }

            List<RadixSortingRecord> sortedRecords = sortingAlgorithm.SortMsd(listToSort);


            await using (StreamWriter writer = new StreamWriter(pathToDestFile, false, Encoding.Default, RecommendedBufferSize))
            {
                foreach (RadixSortingRecord sortedRecord in sortedRecords)
                {
                    await writer.WriteLineAsync(sortedRecord.OriginalValue);
                }
            }

            return pathToDestFile;

        }
          public async Task<string> SortSingleFileWithQuickSort(string pathToSourceFile, string pathToDestFile = null)
        {
            pathToDestFile ??= pathToSourceFile;

            if (File.Exists(pathToSourceFile) == false)
            {
                throw new FileNotFoundException(pathToSourceFile);
            }

            if (File.Exists(pathToDestFile) && pathToDestFile != pathToSourceFile)
            {
                File.Delete(pathToDestFile);
            }


            var notSortedList = new List<SortingRecord>();
            using (StreamReader reader = new StreamReader(pathToSourceFile, Encoding.Default, true, RecommendedBufferSize))
            {
                while (reader.EndOfStream == false)
                {
                    var nextLine = await reader.ReadLineAsync();
                    var nextRecord = new SortingRecord(nextLine);
                    notSortedList.Add(nextRecord);
                }
            }



            if (notSortedList.Any())
            {
               
                List<SortingRecord> sortedRecords = notSortedList.OrderBy(x => x.Text).ThenBy(x => x.Number).ToList();

                await using (StreamWriter writer =
                    new StreamWriter(pathToDestFile, false, Encoding.Default, RecommendedBufferSize))
                {
                    foreach (SortingRecord sortedRecord in sortedRecords)
                    {
                        await writer.WriteLineAsync(sortedRecord.OriginalValue);
                    }
                }
            }

            return pathToDestFile;

        }
    }



}
