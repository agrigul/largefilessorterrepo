﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LargeFileSorter.Domain.RadixAndBucketSort.Radix.Splitting
{

    public class SplitByAlphabet
    {
        const int EncodingMinLetter = ' ';
        const int EncodingMaxLetter = '~';

        public const int RecommendedBufferSize = 1024 * 1024;

        public async Task<IList<string>> SplitFiles(string sourceFilePath, long maxFileSize, int letterIndex = 0)
        {
            if (File.Exists(sourceFilePath) == false)
            {
                throw new FileNotFoundException(sourceFilePath);
            }

            IList<string> listOfChunks = new List<string>();
            FileInfo f = new FileInfo(sourceFilePath);
            if (f.Length <= maxFileSize)
            {
                listOfChunks.Add(sourceFilePath);
                return listOfChunks;
            }

            // Skip the same values
            letterIndex = await FindNextDifferentLetterIndex(sourceFilePath, letterIndex);


            string directory = Path.GetDirectoryName(sourceFilePath);
            string originalFileName = Path.GetFileName(sourceFilePath);


            Dictionary<char, string> sortedPathsToChunkFiles = new Dictionary<char, string>();
            for (int i = EncodingMinLetter; i < EncodingMaxLetter; i++)
            {
                sortedPathsToChunkFiles.Add((char)i, null);
            }

            IDictionary<string, StreamWriter> streamWriters = new Dictionary<string, StreamWriter>();


            try
            {
                using (StreamReader reader =
                    new StreamReader(sourceFilePath, Encoding.Default, true, RecommendedBufferSize))
                {


                    while (reader.EndOfStream == false)
                    {

                        var nextLine = await reader.ReadLineAsync().ConfigureAwait(false);
                        var record = new RadixSortingRecord(nextLine);

                        char letter;
                        if (letterIndex >= record.Text.Length)
                        {
                            letter = ' ';
                        }
                        else
                        {
                            letter = record.Text[letterIndex];
                        }

                        StreamWriter writer = null;
                        if (sortedPathsToChunkFiles[letter] != null)
                        {
                            var path = sortedPathsToChunkFiles[letter];
                            writer = streamWriters[path];
                        }
                        else
                        {
                            var splitFileName = originalFileName.Replace(".txt", "_" + letter + ".txt");
                            var splitFilePath = Path.Combine(directory, splitFileName);
                            writer = new StreamWriter(splitFilePath, true, Encoding.Default, RecommendedBufferSize);
                            sortedPathsToChunkFiles[letter] = splitFilePath;
                            streamWriters.Add(splitFilePath, writer);
                        }

                        await writer.WriteLineAsync(nextLine);

                    }


                }
            }
            finally
            {
                foreach (var pair in streamWriters)
                {
                    if (pair.Value != null)
                    {
                        await pair.Value.DisposeAsync();
                    }
                }

            }

            var result = new List<string>();
            listOfChunks = sortedPathsToChunkFiles.Where(x => x.Value != null).Select(x => x.Value).ToList();


            if (listOfChunks.Count > 1)
            {
                for (var i = 0; i < listOfChunks.Count; i++)
                {
                    var splitFile = listOfChunks[i];
                    var subFiles = await SplitFiles(splitFile, maxFileSize, letterIndex + 1);
                    result.AddRange(subFiles);
                }
                File.Delete(sourceFilePath);
            }
            else
            {
                var chunkToDelete = listOfChunks.FirstOrDefault();
                if (File.Exists(chunkToDelete))
                {
                    File.Delete(chunkToDelete);
                }
                return new List<string> { sourceFilePath };
            }


            return result;
        }


        public async Task<string> MergeFiles(IList<string> listOfSortedFilesPaths, string destFilePath)
        {
            if (listOfSortedFilesPaths.Count == 1)
            {
                return destFilePath;
            }
            if (File.Exists(destFilePath))
            {
                File.Delete(destFilePath);
            }
            File.Copy(listOfSortedFilesPaths.First(), destFilePath);

            await using (StreamWriter writer = new StreamWriter(destFilePath, true, Encoding.Default, RecommendedBufferSize))
            {
                for (int i = 1; i < listOfSortedFilesPaths.Count; i++)
                {
                    using (StreamReader reader = new StreamReader(listOfSortedFilesPaths[i], Encoding.Default, true,
                        RecommendedBufferSize))
                    {
                        while (reader.EndOfStream == false)
                        {
                            var line = await reader.ReadLineAsync();
                            await writer.WriteLineAsync(line);
                        }
                    }

                }
            }
            
            foreach (string filesPath in listOfSortedFilesPaths)
            {

                _ = Task.Run(() =>
                {
                    if (File.Exists(filesPath))
                    {
                        File.Delete(filesPath);
                    }
                });

            }
            return destFilePath;

        }
        private static async Task<int> FindNextDifferentLetterIndex(string sourceFilePath, int letterIndex)
        {
            bool indexContainsDifferentValues = false;
            while (indexContainsDifferentValues == false)
            {
                using (StreamReader reader =
                    new StreamReader(sourceFilePath, Encoding.Default, true, RecommendedBufferSize))
                {
                    var first = await reader.ReadLineAsync();
                    var firstRecord = new RadixSortingRecord(first);
                    while (reader.EndOfStream == false && indexContainsDifferentValues == false)
                    {
                        var next = await reader.ReadLineAsync();
                        var nextRecord = new RadixSortingRecord(next);

                        if (nextRecord.Text.Length > letterIndex && firstRecord.Text.Length > letterIndex)
                        {
                            if (nextRecord.Text[letterIndex] != firstRecord.Text[letterIndex])
                            {
                                indexContainsDifferentValues = true;
                            }
                        }
                        else
                        {
                            if (firstRecord.Text.Length <= letterIndex || nextRecord.Text.Length <= letterIndex)
                            {
                                indexContainsDifferentValues = true;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }

                if (indexContainsDifferentValues == false)
                {
                    letterIndex++;
                }
            }

            return letterIndex;
        }
    }
}
