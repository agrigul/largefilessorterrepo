﻿namespace LargeFileSorter.Domain.RadixAndBucketSort.Radix
{
    public class RadixSortingRecord
    {
        public string Number { get; }
        public string Text { get; }
        public string OriginalValue { get; }//=> Number + "." + Text;

        public RadixSortingRecord(string value)
        {
            var dotIndex = value.IndexOf('.');
            Number = value.Substring(0, dotIndex);
            Text = value.Substring(dotIndex + 1, value.Length - dotIndex - 1).TrimStart();
            OriginalValue = value;
        }
    }
}