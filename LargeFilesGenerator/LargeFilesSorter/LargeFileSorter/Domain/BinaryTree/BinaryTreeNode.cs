﻿using System.Collections.Generic;
using LargeFileSorter.Domain.MergeSort.InMemorySort;

namespace LargeFileSorter.Domain.BinaryTree
{
    
    public class BinaryTreeNode<T> where T : SortingRecord
    {
        protected readonly SortingRecordComparer Comparer = new SortingRecordComparer();
        public BinaryTreeNode<T> Parent { get; private set; }
        public BinaryTreeNode<T> Left { get; private set; }
        public BinaryTreeNode<T> Right { get; private set; }
        public int NumberOfDuplicates { get; private set; } = 1;

        public T NodeValue;
        private readonly List<T> listForPrinting = new List<T>();

        public BinaryTreeNode(T nodeValue, BinaryTreeNode<T> parent)
        {
            NodeValue = nodeValue;
            Parent = parent;
        }

        public void Add(T nextValue)
        {
            if (Comparer.Compare(nextValue, NodeValue) < 0)
            {
                if (Left == null)
                {
                    Left = new BinaryTreeNode<T>(nextValue, this);
                }
                else
                {
                    Left?.Add(nextValue);
                }
            }
            else
            {
                if (Parent != null && Comparer.Compare(nextValue, Parent.NodeValue) == 0)
                {
                    NumberOfDuplicates++;
                }
                else
                {
                    if (Right == null)
                    {
                        Right = new BinaryTreeNode<T>(nextValue, this);
                    }
                    else
                    {
                        Right?.Add(nextValue);
                    }
                }
            }
        }

        public override string ToString()
        {
            return NodeValue.OriginalValue;
        }

        public BinaryTreeNode<T> Search(T value)
        {
            return Search(this, value);
        }

        public bool Remove(T valueToRemove)
        {
            BinaryTreeNode<T> tree = Search(valueToRemove);
            if (tree == null)
            {
                return false;
            }
            BinaryTreeNode<T> currentTree;

            // remove the root
            if (tree == this)
            {
                if (tree.Right != null)
                {
                    currentTree = tree.Right;
                }
                else currentTree = tree.Left;

                while (currentTree.Left != null)
                {
                    currentTree = currentTree.Left;
                }
                T temp = currentTree.NodeValue;
                Remove(temp);
                tree.NodeValue = temp;

                return true;
            }

            //Remove Nodes
            if (tree.Left == null &&
                tree.Right == null &&
                tree.Parent != null)
            {
                if (tree == tree.Parent.Left)
                {
                    tree.Parent.Left = null;
                }
                else
                {
                    tree.Parent.Right = null;
                }
                return true;
            }

            // remove node which has left sub-tree, and has no right tree
            if (tree.Left != null && tree.Right == null)
            {
                //Switch parent
                tree.Left.Parent = tree.Parent;
                if (tree == tree.Parent.Left)
                {
                    tree.Parent.Left = tree.Left;
                }
                else if (tree == tree.Parent.Right)
                {
                    tree.Parent.Right = tree.Left;
                }
                return true;
            }

            // rempve node, which has right sub-tree, but has no left tree
            if (tree.Left == null && tree.Right != null)
            {
                tree.Right.Parent = tree.Parent;
                if (tree == tree.Parent.Left)
                {
                    tree.Parent.Left = tree.Right;
                }
                else if (tree == tree.Parent.Right)
                {
                    tree.Parent.Right = tree.Right;
                }
                return true;
            }

            // remove node which has sub-trees from both sides
            if (tree.Right != null && tree.Left != null)
            {
                currentTree = tree.Right;

                while (currentTree.Left != null)
                {
                    currentTree = currentTree.Left;
                }

                // a very left element is the first child
                if (currentTree.Parent == tree)
                {
                    currentTree.Left = tree.Left;
                    tree.Left.Parent = currentTree;
                    currentTree.Parent = tree.Parent;
                    if (tree == tree.Parent.Left)
                    {
                        tree.Parent.Left = currentTree;
                    }
                    else if (tree == tree.Parent.Right)
                    {
                        tree.Parent.Right = currentTree;
                    }
                    return true;
                }
                //a very left element is NOT the first child
                else
                {
                    if (currentTree.Right != null)
                    {
                        currentTree.Right.Parent = currentTree.Parent;
                    }

                    currentTree.Parent.Left = currentTree.Right;
                    currentTree.Right = tree.Right;
                    currentTree.Left = tree.Left;
                    tree.Left.Parent = currentTree;
                    tree.Right.Parent = currentTree;
                    currentTree.Parent = tree.Parent;

                    if (tree == tree.Parent.Left)
                    {
                        tree.Parent.Left = currentTree;
                    }
                    else if (tree == tree.Parent.Right)
                    {
                        tree.Parent.Right = currentTree;
                    }

                    return true;
                }
            }
            return false;
        }

        public List<T> Print()
        {
            listForPrinting.Clear();
            Print(this);

            return listForPrinting;
        }

        private BinaryTreeNode<T> Search(BinaryTreeNode<T> tree, T value)
        {
            if (tree == null) return null;
            switch (Comparer.Compare(value, tree.NodeValue))
            {
                case 1: return Search(tree.Right, value);
                case -1: return Search(tree.Left, value);
                case 0: return tree;
                default: return null;
            }
        }

        private void Print(BinaryTreeNode<T> tree)
        {
            if (tree == null)
            {
                return;
            }

            Print(tree.Left);

            int numberOfDuplicates = tree.NumberOfDuplicates;
            while (numberOfDuplicates > 0)
            {
                listForPrinting.Add(tree.NodeValue);
                numberOfDuplicates--;
            }


            if (tree.Right != null)
            {
                Print(tree.Right);
            }
        }


    }

}
