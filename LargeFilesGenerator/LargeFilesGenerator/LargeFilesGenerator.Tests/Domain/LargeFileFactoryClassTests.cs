﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using LargeFilesGenerator.Domain;
using NUnit.Framework;

namespace LargeFilesGenerator.Tests.Domain
{
    [TestFixture]
    public class LargeFileFactoryClassTests
    {
        private string testDatFolderName = "TestsFiles";
        private string testDataDirectoryPath;

        public LargeFileFactoryClassTests()
        {
            testDataDirectoryPath = Path.Combine(Directory.GetCurrentDirectory(), testDatFolderName);
        }

        [SetUp]
        public void Setup()
        {

            if (Directory.Exists(testDataDirectoryPath))
            {
                Directory.Delete(testDataDirectoryPath, true);
            }

            Thread.Sleep(1000);
            Directory.CreateDirectory(testDataDirectoryPath);

            Assert.IsTrue(Directory.Exists(testDataDirectoryPath));

        }

        [TestCase(50)]
        [TestCase(100)]
        [TestCase(100020)]
        [TestCase(1100000000)] // 9 sec
        [TestCase(5000000000)] // 55-60sec
        [Description("Should generate data and save to file")]
        public async Task Void_FileContainsGeneratedData(long fileSize)
        {
            LargeFileFactory factory = new LargeFileFactory();

            string pathTestFile = Path.Combine(testDataDirectoryPath, $"TestGeneratedFile_{DateTime.Now.Ticks}.txt");

            var start = DateTime.Now;
            await factory.GenerateDataAndWriteToFileAsync(pathTestFile, fileSize);
            var end = DateTime.Now;

            var diff = (end - start);
            Console.WriteLine(diff.Ticks);
            Console.WriteLine("Total: " + diff.TotalSeconds);

            Assert.IsTrue(File.Exists(pathTestFile));

            var realSize = new FileInfo(pathTestFile).Length;

            Assert.AreEqual(fileSize, realSize, fileSize / 10);

        }
    }
}
