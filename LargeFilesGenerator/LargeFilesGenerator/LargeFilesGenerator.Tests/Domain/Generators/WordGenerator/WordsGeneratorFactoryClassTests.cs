﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LargeFilesGenerator.Domain.Generators;
using LargeFilesGenerator.Domain.Generators.WordsGenerator;
using NUnit.Framework;

namespace LargeFilesGenerator.Tests.Domain.Generators.WordGenerator
{
    [TestFixture]
    public class WordsGeneratorFactoryClassTests
    {
        private readonly NumbersGeneratorService numbersGenerator;
        private readonly WordsGeneratorFactory factoryUnderTest;
        public WordsGeneratorFactoryClassTests()
        {
            numbersGenerator = new NumbersGeneratorService();
            factoryUnderTest = new WordsGeneratorFactory(numbersGenerator);
        }

        [Test]
        [Description("Should create a phrase with number and words from table")]
        public void CreateNewPhrase_Void_ContainsValueFromTable()
        {
            var result = factoryUnderTest.CreateNewPhraseAsString().ToString();

            Assert.True(WordsGeneratorFactory.FruitsAndVegetablesTable.Any(x => result.Contains(x)));
            Assert.True(WordsGeneratorFactory.ColorsTable.Any(x => result.Contains(x)));
            Assert.True(WordsGeneratorFactory.AdjectivesTable.Any(x => result.Contains(x)));
        }

        [Test]
        [Description("Should create phrase with numbers and then words")]
        public void CreateNewPhrase_Void_NumbersFirstThenWords()
        {
            var result = factoryUnderTest.CreateNewPhraseAsString().ToString();

            Regex regex = new Regex(@"[0-9]*\. [\w ]*", RegexOptions.IgnoreCase | RegexOptions.Compiled);

            Assert.IsTrue(regex.IsMatch(result));
        }


        [Test]
        [Description("Should create (almost)unique phrases in parallel.")]
        public async Task CreateNewPhrase_Multithread_CreatesDifferentPhrases()
        {

            const int tasksCount = 5000000;
            Task[] allTasks = new Task[tasksCount];

            var start = DateTime.Now;
            for (int i = 0; i < tasksCount; i++)
            {
                var task = new Task<string>( () => factoryUnderTest.CreateNewPhraseAsString());
                allTasks[i] = task;
            }

            Parallel.ForEach(allTasks, t => t.Start());
            Task.WaitAll(allTasks);
            var end = DateTime.Now;
            var diff = end - start;

            HashSet<string> hasSet = new HashSet<string>();

            foreach (var task in allTasks)
            {
                var stringTask = (Task<string>)task;
                var phrase = await stringTask.ConfigureAwait(false);
                Assert.IsNotNull(phrase);
                hasSet.Add(phrase); 
            }
         
            Console.WriteLine("Total time:" + diff.TotalSeconds);
            
            Assert.AreEqual(tasksCount, hasSet.Count, tasksCount / 10);

        }
    }
}
