﻿using LargeFilesGenerator.Domain.Generators;
using NUnit.Framework;

namespace LargeFilesGenerator.Tests.Domain.Generators
{
    public class NumbersGeneratorServiceClassTests
    {

        [Test]
        [Description("Should return randomizer")]
        public void GetRandomizer_Void_ReturnsRandomizer()
        {
            var serviceUnderTest = new NumbersGeneratorService();
            var result = serviceUnderTest.GetRandomizer();

            Assert.IsNotNull(result);
        }


        [Test]
        [Description("Should create new instance")]
        public void GetRandomizer_Void_ReturnsNewInstance()
        {
            var serviceUnderTest = new NumbersGeneratorService();
            var result1 = serviceUnderTest.GetRandomizer();
            var result2 = serviceUnderTest.GetRandomizer();

            Assert.AreNotEqual(result2, result1);
        }
    }
}
