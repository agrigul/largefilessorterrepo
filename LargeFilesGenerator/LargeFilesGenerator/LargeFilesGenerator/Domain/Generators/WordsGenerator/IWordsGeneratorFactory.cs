﻿namespace LargeFilesGenerator.Domain.Generators.WordsGenerator
{
    public interface IWordsGeneratorFactory
    {
        string CreateNewPhraseAsString();
    }
}
