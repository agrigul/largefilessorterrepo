﻿using System;

namespace LargeFilesGenerator.Domain.Generators.WordsGenerator
{
    public class WordsGeneratorFactory : IWordsGeneratorFactory
    {
        private readonly INumbersGeneratorService numbersGenerator;

        public WordsGeneratorFactory(INumbersGeneratorService numbersGenerator)
        {
            this.numbersGenerator = numbersGenerator ?? throw new ArgumentNullException(nameof(numbersGenerator));
        }

        private const int FruitsAndVegetablesTableCount = 11;
        public static readonly string[] FruitsAndVegetablesTable =
        {
            "Apple",
            "Apricot",
            "Banana",
            "Cucumber",
            "Carrot",
            "Cherry",
            "Onion",
            "Pineapple",
            "Potato",
            "Strawberry",
            "Tomato"
        };

        private const int ColorsTableCount = 11;
        public static readonly string[] ColorsTable =
        {
            "Red",
            "Orange",
            "Yellow",
            "Green",
            "Blue",
            "Dark blue",
            "Violate",
            "Pink",
            "Black",
            "White",
            "Grey"
        };

        private const int AdjectivesTableCount = 6;
        public static readonly string[] AdjectivesTable =
        {
            "Tasty",
            "Sweet",
            "Bitter",
            "Sour",
            "Salty",
            "Delicate",
        };

        //public string CreateNewPhraseAsString()
        //{
        //    Random rnd = numbersGenerator.GetRandomizer();

        //    int nextFruitIndex = rnd.Next(FruitsAndVegetablesTableCount - 1);
        //    int nextColorIndex = rnd.Next(ColorsTableCount - 1);
        //    int nextAdjectiveIndex = rnd.Next(AdjectivesTableCount - 1);

        //    int number = rnd.Next();
        //    var builder = new StringBuilder(number.ToString()).Append('.')
        //        .Append(ColorsTable[nextColorIndex]).Append(' ')
        //        .Append(AdjectivesTable[nextAdjectiveIndex]).Append(' ')
        //        .Append(FruitsAndVegetablesTable[nextFruitIndex]);

        //    return builder.ToString();
        //}

        public string CreateNewPhraseAsString()
        {
            Random rnd = numbersGenerator.GetRandomizer();
            int number = rnd.Next();

            int nextFruitIndex = rnd.Next(FruitsAndVegetablesTableCount - 1);
            int nextColorIndex = rnd.Next(ColorsTableCount - 1);
            int nextAdjectiveIndex = rnd.Next(AdjectivesTableCount - 1);


            var builder = number + ". " + ColorsTable[nextColorIndex] + " " + AdjectivesTable[nextAdjectiveIndex] + " " +
                          FruitsAndVegetablesTable[nextFruitIndex];
            return builder;
        }
    }
}