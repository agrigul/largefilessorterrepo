﻿using System;
using System.Threading;

namespace LargeFilesGenerator.Domain.Generators
{
    public class NumbersGeneratorService : INumbersGeneratorService
    {
        private static int seed = Environment.TickCount;

        private static readonly ThreadLocal<Random> ThreadVariable =
            new ThreadLocal<Random>(() =>

            new Random(Interlocked.Increment(ref seed))
        );
        
        public Random GetRandomizer()
        {
            return ThreadVariable.Value;
        }
    }
}
