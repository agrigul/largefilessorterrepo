﻿using System;

namespace LargeFilesGenerator.Domain.Generators
{
    public interface INumbersGeneratorService
    {
        Random GetRandomizer();
    }
}