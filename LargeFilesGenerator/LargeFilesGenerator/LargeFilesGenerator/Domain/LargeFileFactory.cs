﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LargeFilesGenerator.Domain.Generators;
using LargeFilesGenerator.Domain.Generators.WordsGenerator;

namespace LargeFilesGenerator.Domain
{
    public class LargeFileFactory : ILargeFileFactory
    {

        /// <summary>
        /// Generates file of the provided size. Uses 4 files to parallel write.
        /// </summary>
        /// <param name="pathToFile"></param>
        /// <param name="maxFileSize"></param>
        /// <returns></returns>
        public async Task GenerateDataAndWriteToFileAsync(string pathToFile, long maxFileSize)
        {
            
            long sizeOfPortion = maxFileSize / 4;
            int minSizeOfPortion = 10000;
            if (sizeOfPortion < minSizeOfPortion)
            {
                await GenerateDataAndWriteToSingleFileAsync(pathToFile, maxFileSize);
                return;
            }
            

            string tempFile1 = pathToFile.Replace(".txt", "_temp1.txt");
            string tempFile2 = pathToFile.Replace(".txt", "_temp2.txt");
            string tempFile3 = pathToFile.Replace(".txt", "_temp3.txt");
            string tempFile4 = pathToFile.Replace(".txt", "_temp4.txt");

            var start = DateTime.Now;
            var task1 = GenerateDataAndWriteToSingleFileAsync(tempFile1, sizeOfPortion);
            var task2 = GenerateDataAndWriteToSingleFileAsync(tempFile2, sizeOfPortion);
            var task3 = GenerateDataAndWriteToSingleFileAsync(tempFile3, sizeOfPortion);
            var task4 = GenerateDataAndWriteToSingleFileAsync(tempFile4, sizeOfPortion);

            await Task.WhenAll(task1, task2, task3, task4).ConfigureAwait(false);
            var generateTime = DateTime.Now - start;

            var fileRepository = new FileRepository();

            start = DateTime.Now;
            task1 = Task.Run(() => fileRepository.AppendFile(tempFile2, tempFile1));
            task2 = Task.Run(() => fileRepository.AppendFile(tempFile4, tempFile3));

            await Task.WhenAll(task1, task2);

            fileRepository.AppendFile(tempFile3, tempFile1);
            File.Move(tempFile1, pathToFile);

            var merged = DateTime.Now - start;


            Console.WriteLine($"\n\n#### File of size {maxFileSize / 1024 / 1024} MB was generated ###");
            Console.WriteLine($"Generated: {generateTime.TotalSeconds} seconds.\n" +
                              $"Merge: {merged.TotalSeconds} seconds\n");
            Console.WriteLine($"\nTotal time: {(generateTime + merged).TotalSeconds}");
        }


        /// <summary>
        /// Generates values for single file and writes portions one by one
        /// </summary>
        /// <param name="pathName"></param>
        /// <param name="maxFileSize"></param>
        /// <returns></returns>
        private async Task GenerateDataAndWriteToSingleFileAsync(string pathName, long maxFileSize)
        {
            INumbersGeneratorService numbersGenerator = new NumbersGeneratorService();
            var wordsFactory = new WordsGeneratorFactory(numbersGenerator);


            long currentSize = 0;
            long currentPortionSize = 0;
            bool isMaxSize = false;


            var fileRepository = new FileRepository();

            var queue = new ConcurrentQueue<string>();
            var tasks = new List<Task>();

            const long maxFlushSize = 200000000; //NOTE: write to file every 200 mbs

            while (isMaxSize == false)
            {

                for (int i = 0; i < 1000; i++)
                {
                    var task = Task.Run(() =>
                    {
                        if (currentPortionSize > maxFlushSize || currentSize > maxFileSize)
                        {
                            // Time to write portion
                            return;
                        }
                        var nextLine = wordsFactory.CreateNewPhraseAsString();
                        Interlocked.Add(ref currentPortionSize, nextLine.Length);
                        Interlocked.Add(ref currentSize, nextLine.Length);

                        queue.Enqueue(nextLine);

                    });

                    if (currentPortionSize > maxFlushSize)
                    {
                        // Time to write portion
                        break;
                    }

                    tasks.Add(task);

                }

                await Task.WhenAll(tasks.ToArray());

                currentSize = fileRepository.GetFileSize(pathName);
                var queueSize = queue.Sum(x => x.Length);

                if (currentSize + queueSize >= maxFileSize)
                {
                    isMaxSize = true;
                    break;
                }

                await fileRepository.WriteToFileAsync(pathName, queue);

                currentPortionSize = 0;
            }


            // Write tail. The final size of the file may be +- 1000 kb less or more, then requested.
            ConcurrentQueue<string> tail = new ConcurrentQueue<string>();
            while (currentSize < maxFileSize && queue.Any())
            {
                queue.TryDequeue(out string tailLine);
                if (tailLine != null)
                {
                    if (currentSize + tailLine.Length < maxFileSize)
                    {
                        tail.Enqueue(tailLine);
                        currentSize += tailLine.Length;
                    }
                }
            }

            if (tail.Any())
            {
                await fileRepository.WriteToFileAsync(pathName, tail);
            }

        }
        
    }
}