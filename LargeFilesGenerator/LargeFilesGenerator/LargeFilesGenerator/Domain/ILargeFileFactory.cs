﻿using System.Threading.Tasks;

namespace LargeFilesGenerator.Domain
{
    public interface ILargeFileFactory
    {
        Task GenerateDataAndWriteToFileAsync(string pathToFile, long maxFileSize);
    }
}