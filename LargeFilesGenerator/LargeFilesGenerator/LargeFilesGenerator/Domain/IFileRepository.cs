﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LargeFilesGenerator.Domain
{
    public interface IFileRepository
    {
        Task WriteToFileAsync(string path, ICollection<StringBuilder> linesWithWords, int bufferSize = short.MaxValue);
        Task WriteToFileAsync(string path, ICollection<string> linesWithWords, int bufferSize = short.MaxValue);
        Task WriteToFileAsync(string path, ConcurrentQueue<string> linesWithWords, int bufferSize = short.MaxValue);
        long GetFileSize(string path);
    }
}