﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace LargeFilesGenerator.Domain
{
    public class FileRepository : IFileRepository
    {

        public void AppendFile(string sourceFilePath, string destFilePath)
        {
            if (File.Exists(sourceFilePath) == false)
            {
                throw new FileNotFoundException(sourceFilePath);
            }

            if (File.Exists(destFilePath) == false)
            {
                throw new FileNotFoundException(destFilePath);
            }

            using (FileStream source = File.Open(sourceFilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (FileStream dest = File.Open(destFilePath, FileMode.Append, FileAccess.Write))
                {
                    source.CopyTo(dest);
                }
            }

            File.Delete(sourceFilePath);
        }

        public async Task WriteToFileAsync(string path, ICollection<StringBuilder> linesWithWords, int bufferSize = short.MaxValue)
        {
            StringBuilder finalBuilder = new StringBuilder();
            foreach (StringBuilder line in linesWithWords)
            {
                finalBuilder.Append(line).Append(Environment.NewLine);
            }

            using (StreamWriter writer = new StreamWriter(path, true, Encoding.Default, bufferSize))
            {
                await writer.WriteAsync(finalBuilder).ConfigureAwait(false);
            }
        }


        public async Task WriteToFileAsync(string path, ICollection<string> linesWithWords, int bufferSize = short.MaxValue)
        {
            using (StreamWriter writer = new StreamWriter(path, true, Encoding.Default, bufferSize))
            {
                foreach (string line in linesWithWords)
                {
                    await writer.WriteLineAsync(line).ConfigureAwait(false);
                }
            }
        }

        public async Task WriteToFileAsync(string path, ConcurrentQueue<string> linesWithWords, int bufferSize = short.MaxValue)
        {
            using (StreamWriter writer = new StreamWriter(path, true, Encoding.Default, bufferSize))
            {
                foreach (string line in linesWithWords)
                {
                    await writer.WriteLineAsync(line).ConfigureAwait(false);
                }
            }
        }

        public long GetFileSize(string path)
        {
            if (File.Exists(path) == false)
            {
                return 0;
            }

            FileInfo info = new FileInfo(path);
            return info.Length;
        }
    }
}
