﻿using System;
using System.IO;
using LargeFilesGenerator.Domain;

namespace LargeFilesGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("\nThis is a test task service made by Artem Griguletskiy for interview." +
                                  "\nAll Rights reserved. Algorithm, code or program can't be used for any purposes, but once time for interview.\n");

                var defaultPathToFile = Path.Combine(Directory.GetCurrentDirectory(), "TestFiles", "TestGeneratedFile.txt");
                Console.WriteLine(
                    $"\nPlease, enter path (directory and file name) to a generate file. If nothing was entered the '{defaultPathToFile}' file will be used\n");
                string pathToFile = null;
                string dir = null;

                while (Directory.Exists(dir) == false || string.IsNullOrWhiteSpace(pathToFile))
                {
                    pathToFile = Console.ReadLine();

                    if (string.IsNullOrWhiteSpace(pathToFile))
                    {
                        pathToFile = defaultPathToFile;
                        Console.WriteLine("Next path will be used:\n");
                        Console.WriteLine(pathToFile);
                        Console.WriteLine();
                    }
                    else
                    {
                        if (pathToFile.Contains(".txt") == false)
                        {
                            pathToFile += Path.Combine(pathToFile, "TestGeneratedFile.txt");
                        }
                    }

                    if (File.Exists(pathToFile))
                    {
                        Console.WriteLine($"\nThe existing file with the same name was found and will be removed at: {pathToFile}.\n");

                        File.Delete(pathToFile);
                    }

                    dir = Path.GetDirectoryName(pathToFile);
                    if (Directory.Exists(dir) == false)
                    {
                        Console.WriteLine($"\nThis directory didn't exist and was created: {dir}.");
                        Directory.CreateDirectory(dir);
                    }
                }

                bool allIsDone = false;
                while (allIsDone == false)
                {

                    Console.WriteLine("\nPlease, specify the size of the file in MB");
                    long fileSize = 0;
                    var isValidSize = false;
                    while (isValidSize == false)
                    {
                        var fileSizeAsString = Console.ReadLine();
                        if (long.TryParse(fileSizeAsString, out fileSize))
                        {
                            if (fileSize > 0)
                            {
                                fileSize = fileSize * 1024 * 1024;
                            }
                            else
                            {
                                Console.WriteLine("\nPlease, specify the correct size of the file in MB");
                            }

                            isValidSize = true;
                        }

                    }

                    if (File.Exists(pathToFile))
                    {
                        File.Delete(pathToFile);
                    }

                    ILargeFileFactory service = new LargeFileFactory();
                    Console.WriteLine("\nGenerating....");
                    service.GenerateDataAndWriteToFileAsync(pathToFile, fileSize).Wait();


                    Console.WriteLine("\nThe file was generated at: ");
                    Console.WriteLine($"\n{pathToFile}");

                    Console.WriteLine("\nDo you want to recreate the same file? (y\\n)");
                    var recreate = Console.ReadLine();
                    allIsDone = recreate != "y";

                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"\nThere is an exception: {e.Message}");
                Console.ReadLine();
                Console.WriteLine("\nAll details:");
                Console.WriteLine(e);
                Console.ReadLine();
                throw;
            }
        }
    }
}
